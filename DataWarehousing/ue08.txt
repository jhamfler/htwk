﻿8 a

select c.riskid,
	count(distinct s.customerNo) konten,
	count(distinct s.orderno) bestellungen,
	sum (s.quantity) Artikel,
	sum(s.amount * s.quantity) nettosumme
from iw_customer c, iw_sales s
where c.customerNo = s.customerNo
group by c.riskid


 b

select c.riskid,
	count(distinct s.customerNo) konten,
	count(distinct s.orderno) bestellungen,
	sum (s.quantity) Artikel,
	sum(s.amount * s.quantity) nettosumme,
	count(distinct r1.returnno) retouren,
	sum(r1.quantity) ret_artikel,
	sum(r1.line_amount) ret_nettowert

from iw_customer c, iw_sales s
left outer join iw_return_line r1 on s.customerno = r1.customerNo
where c.customerNo = s.customerNo
group by c.riskid

c-alpha

select c.riskid,
	count(distinct s.customerNo) konten,
	count(distinct s.orderno) bestellungen,
	sum (s.quantity) Artikel,
	cast (sum(s.amount * s.quantity) as decimal (10,2))nettosumme,
	count(distinct r1.returnno) retouren,
	sum(r1.quantity) ret_artikel,
	sum(r1.line_amount) ret_nettowert

from iw_customer c, iw_sales s
left outer join iw_return_line r1 on s.customerno = r1.customerNo
where c.customerNo = s.customerNo and s.type=2 and r1.type=2
group by c.riskid


c - mit subquery behält outerjoin ---- unvollständig???
select a.riskid,
	count(distinct a.customerNo) konten,
	sum(a.bestellungen) bestellungen,
	sum(a.artikel) artikel,
	sum(a.nettosumme) nettosumme,
	sum(b.retouren) retouren,
	sum(b.ret_artikel)ret_artikel
from 
(
	select
		c.riskid,
		s.customerno,
		count(s.orderno) bestellungen,
		sum(s.quantity) artikel,
		sum(s.amount * s.quantity) nettosumme
	from iw_customer c, iw_sales s
	where c.customerNo = s.customerNo and s.type=2
	group by c.riskID, s.customerNo
) a
left outer join 
(
	select 
		r1.customerno,
		count(distinct r1.returnNo) retouren,
		sum(r1.quantity) ret_artikel,
		sum(r1.line_amount) nettowert
	from iw_return_line r1
	where r1.type=2
	group by r1.customerNo
) b
 on a.customerno = b.customerNo
group by a.riskid

