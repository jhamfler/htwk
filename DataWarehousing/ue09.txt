tabelle anlegen von zieldatenbank aus ------------------------------------

select * into Kundenreport
from iw_shop.dbo.iw_sales




create view produktgruppe ----------------------------------------------
create view dwh1702.pg_view with schemabinding as 
select p.productgroup,
	datepart(yyyy,s.postingdate) Jahr,
	datepart(mm,s.postingdate) Monat,
	cast(sum(s.quantity*s.amount) as decimal(10,2)) Umsatz,
	count_big(*) AnzahlVerkäufe,
from iw_shop.dbo.iw_sales s, iw_shop.dbo.iw_article p
where s.IWAN = p.IWAN
group by 
	p.productgroup,
	datepart(yyyy,s.postingadate),
	datepart(mm,s.postingdate);


materialisierte sicht ------------------------------------------------------
physische speicherung der tupel in der sicht, view ist nur ausführung der query in der DB ----------------------------

create unique clustered index pgindex on
dwh1702.pgview (produktgroup, monat, jahr);




select p.productgroup, datepart(yyyy,s.postingdate) Jahr,
	sum(s.quantity * s.amount) Umsatz,
	count_big(*) Anzahl
from iw_sales s, iw_article p
where s.IWAN = p-IWAN and p.productgroup between 150 and 225
group by p.productgroup, datepart(yyyy,s.postingdate)
order by p.productgroup, Jahr




select pv.productgroup, pv.Jahr, sum(pv.Umsatz) Jahresumsatz
from dqh1702.pg_view pv, iw_article p
where 
	pv.productgroup = p.productgroup and
	p.productgroup between 150 and 225
group by pv.productgroup, pv.Jahr
order by pv.productgroup