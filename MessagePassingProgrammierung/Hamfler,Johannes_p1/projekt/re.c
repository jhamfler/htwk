#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <math.h>
#include <mpi.h>
#include <unistd.h>
#include <time.h>

#define MAXLINE 1024 // für PBM
#include "pbm.h"

// Struct für Teilrechtecke
struct rechteck {
	int xstart;
	int ystart;
	int xende;
	int yende;
	int fehler;
};

// Prototypen
void printmode(int modus);
void uninit(int code, int size, int x, int rechtecke, struct timespec startt, struct timespec start2t, clockid_t clk_id);
int findeRechteck(char * feld, int x , int y, char schwarz, struct rechteck * re);

int main(int argc, char ** argv){
	if (argc < 4) {
		printf("Zu wenig Argumente.\n");
		printf("Benutzung: mpirun -hostfile <pfad/hostsdatei> re <Modus> <debugging> <spezielle Optionen>\n");
		printmode(0);
		printmode(1);
		return 1;
	}

	// bestimme wie das Feld erstellt wird
	int mode=1;
	if (atoi(argv[1]) == 0)	mode=0;
	else					mode=1;

	// automatisches Erstellen von Rechtecken benötigt mehr Argumente
	if (mode==0 && argc<5) {
		printmode(0);
		return 1;
	}

	char debug=1;
	debug = atoi(argv[2]) == 0 ? 0 : 1;

	char schwarz='1'; // Wert im Bild, welcher als schwarz erkannt werden soll bzw. als Rechteckfarbe
	char weiss='0';
	int is_ascii=1; // für PBM
	FILE *f;

	// MPI init
	int rank, size, sizeinit; // CPUID & #CPUs
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &sizeinit);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Request req;
	MPI_Status status;
	system("ulimit -s unlimited");	// Stackgröße erhöhen um Segmentation Faults zu verringern

	// Größe des Felds prüfen
	int x,y;
	if (mode == 0) {
		// Eingabefehler erkennen
		x=atoi(argv[3]);
		if (x<1) {
			if (rank==0 && debug) perror("Größe < 1 wird nicht akzeptiert!\n");
			exit(1);
		}
		x=y=atoi(argv[3]); // Größe des Felds in dem Rechtecke sein können
	}
	else {
		f = fopen(argv[3],"r"); // Öffne "Bild"
		if (f==NULL) {
			if (rank==0) {
				perror("Bild existiert nicht.\n");
				exit(2);
			}
			else {
				exit(0);
			}
		}
		read_pbm_header(f, &x, &y, &is_ascii); // Größe herausfinden
		if (rank==0 && debug) printf("x: %d, y: %d\n", x, y); // anzeigen

		if (x != y) {
			if (rank==0 && debug) perror("Kein Quadrat als Eingabedatei.\n");
			exit(3);
		}
	}

	// sicherstellen, dass mindestens 2 Zeilen pro Prozessor bearbeitet werden
	size=sizeinit;
	if (x<2) {
		 size=1;
	}
	else {
		int i=2;
		while (x<size*2) {
			if (rank==0 && debug) printf("Anzahl zur Verfügung stehender Prozessoren: %d\n",size);
			size=x/i;
			i*=2;
		}
	}
	if (size<1) size=1; // sollte nie eintreten


	// Datenstruktur erstellen
	struct rechteck beispielrechteck;
	MPI_Datatype array_of_types[1]; // 1 typ: int
	array_of_types[0] = MPI_INT;

	int array_of_blocklengths[1];
	array_of_blocklengths[0] = 5; // 5 ints

	MPI_Aint array_of_displaysments[1]; // nur int-abstand ist wichtig
	MPI_Aint a1, a2;
	MPI_Get_address(&beispielrechteck       ,&a1);
	MPI_Get_address(&beispielrechteck.xstart,&a2);
	array_of_displaysments[0] = a2 - a1; // offset der ints

	MPI_Datatype rechteck_type; // typ für benutzung kreieren
	MPI_Type_create_struct(1, array_of_blocklengths, array_of_displaysments, array_of_types, &rechteck_type);
	MPI_Type_commit(&rechteck_type); // und setzen


	// Master
	if (rank==0) {
		struct rechteck re[size]; // alle Teilrechtecke
		int z; // Teilgröße und Hilfsvariable
		int rechtecke=-50; // Dummywert, damit man bei der Zeitmessung erkennen kann, dass eine Datei eigelesen wurde
	    char * img = malloc(x*y* sizeof(char));
		if (img == NULL) return 40;

		// wenn kein Bild eingelesen werden soll
		if (!mode) {
		    rechtecke=atoi(argv[4]); // Anzahl Rechtecke
		    int rx,ry,ox,oy; // Zufallszahlen
		    srand(time(NULL)); // Zufallszahlen init mit Seed

			// alles weiß machen (0) oder auch alles schwarz (1), wenn -1
			for (long long i=0; i<x; i++) {
				for (long long j=0; j<y; j++) {
					img[i*x+j] = rechtecke != -1 ? weiss : schwarz;
				}
			}

			// Rechtecke einfügen wenn nicht alles schwarz werden soll
		    if (rechtecke > 0) {
		        for (int k=0; k<rechtecke; k++) {
		            rx = rand()%x; // zufallsgröße der Rechtecke
		            ry = rand()%y;
		            ox = rand()%x; // Offsets der Rechtecke
		            oy = rand()%y;

					// Prüfen ob als Zufallsgröße nicht eine 0 für x & y enstanden ist
					if (rx==0) rx=1;
					if (ry==0) ry=1;

	    	        // Wertebereich nicht überschreiten
	    	        while (rx+ox>x) { rx/=2; ox/=2; }
	    	        while (ry+oy>y) { ry/=2; oy/=2; }

	        	    for (int i=0; i<rx; i++) {
	      	          for (int j=0; j<ry; j++) {
	        	            img[(ox+i)*x+oy+j]=schwarz;
	            	    }
	            	}
	        	}
	    	}
		}
		// Bild einlesen
		else {
			long long i=0;
			i=read_pbm_data(f, img, is_ascii);
			if (debug) printf("Gesamtgröße des Quadrats: %lld\n",i);
            if ((long long)x*y != i) {
				if (debug) perror("Falsch formatierte PBM Datei!\n");
				exit(4);
			}
		}

		if (debug)  {
			printf("Anzahl genutzter CPUs: %d\n", size);
			if (x<30){
				// Feld ausgeben
				for (int i=0; i<x*y; i++) {
					if (i%x==0) printf("\n");
					printf("%c",img[i]);
				}
				printf("\n");
			}
		}

		// Zeitmessung init
		struct timespec startt, start2t;
		clockid_t clk_id;
		clk_id = CLOCK_MONOTONIC_RAW;

		// 2 Zeitpunkte messen
		clock_gettime(clk_id, &startt);
		clock_gettime(clk_id, &start2t);

		// Sende Teildaten an alle Slaves
		for (int i=0, h=0; i<size-1; i++) { // letzte Berechnung übernimmt Master

			// berechne Höhe des Teilproblems
			z = (y*(i+1)/size)  // Anfang des nächsten Problems
			   -(y* i   /size); // Anfang des aktuellen Problems

			// sende vertikale und horizontale Problemgröße
			MPI_Send(&z, 1, MPI_INT, i+1, 0, MPI_COMM_WORLD);
			MPI_Send(&x, 1, MPI_INT, i+1, 1, MPI_COMM_WORLD);

			// sende Teilproblem an Slave i+1
			MPI_Send(img+(y*i/size)*x, z*x, MPI_CHAR, i+1, 2, MPI_COMM_WORLD);
		}

		// Problem für den Masterprozess berechnen (letztes verbleibendes Rechteck)
		z = y - (y*(size-1)/size); // Teilproblemgröße für den Master
		struct rechteck r = {-1,-1,-1,-1}; // Masterrechteck ist das letzte

		// Teilrechteck prüfen:    (y*(size-1)/size)*x ist Anfang des letzten Teilproblems
		r.fehler=findeRechteck(img+(y*(size-1)/size)*x, x, z, schwarz, &r);
		if (r.fehler) {
			if (debug) printf("rank %d meldet: Rechteck ist nicht zusammenhängend!\n",rank);
			uninit(0, size, x, rechtecke, startt, start2t, clk_id);
		}
		else {
			if (debug) printf("rank %d meldet: alles bisher ok.\n",rank);
		}
		re[size-1] = r; // Rechteck des Masters in die Liste einfügen

		// empfange Resultate der Slaves
		MPI_Status status;
		for (int i=0; i<size-1; i++) { // letzte Berechnung hat der Master bereits durchgeführt
			MPI_Recv(&r, 1, rechteck_type, MPI_ANY_SOURCE, 0, MPI_COMM_WORLD, &status);
			re[status.MPI_SOURCE-1]=r;

			if (r.fehler) {
				if (debug) printf("rank %d meldet: Rechteck ist nicht zusammenhängend!\n", status.MPI_SOURCE);
				uninit(0, size, x, rechtecke, startt, start2t, clk_id);
			}
			else {
				if (debug) printf("rank %d meldet: alles bisher ok.\n", status.MPI_SOURCE);
			}
		}

		// wenn kein Fehler in Teilproblemen aufgetreten ist, dann kann der Master prüfen, ob das Rechteck zusammenhängt
		int a1, a2, zusammenhang[size], rechteckgefunden=0, erwarterechteck=1;
		for (int i=0; i<size; i++) {
			// prüfe, ob das Teilproblem ein Rechteck enthält, wenn -1, dann enthält es nur Nullen
			if (re[i].xstart == -1 ||
				re[i].ystart == -1 ||
				re[i].xende  == -1 ||
				re[i].yende  == -1) {
				if (rechteckgefunden) { // ab jetzt darf kein weiteres Rechteck gefunden werden
					erwarterechteck=0; // wenn doch, sind Rechtecke in Teilprobelmen verteilt
				}
				else { // ansonsten ist der Zustand nicht wichtig respektive bisher wurde keins gefunden
					zusammenhang[i]=0;
				}
			}
			else { // ein Rechteck ist vorhanden und muss geprüft werden
				if (rechteckgefunden) { // wenn bereits eines gefunden wurde
					if (erwarterechteck && // und eines erwartet wird
						zusammenhang[i-1]==1) { // vorheriges Rechteck muss an jetziges anstoßen // TODO sollte nicht nötig sein
						// vorheriges Teilproblem enthält Rechteck, prüfe ob es an jetziges anstößt
						a1 = y*(i-1)/size; // Anfang des vorherigen Problems
	                                        a2 = y* i   /size; // Anfang des aktuellen Problems
        	                                if (    re[i-1].xstart  == re[i].xstart &&              // linke Seite
                	                                re[i-1].xende   == re[i].xende  &&              // rechte Seite
                        	                        re[i-1].yende + a1 + 1 == re[i].ystart + a2) {  // Ende i (oberes Rechteck) == Anfang i+1 (unteres Rechteck)
                                	                zusammenhang[i]=1; // rechteck hängt zusammen
                                        	}
						else { // Rechteck hängt nicht zusammen
							if (debug) printf("Rechteck hängt nicht zusammen!\n");
							uninit(0, size, x, rechtecke, startt, start2t, clk_id);
						}

					}
					else { // wenn ein Rechteck gefunden wurde, aber keins erwatet wird herrscht Fragmentierung
						if (debug) printf("Rechtecke sind in nicht benachbarten Teilproblemen verteilt und hängen nicht zusammen!\n");
						uninit(0, size, x, rechtecke, startt, start2t, clk_id);
					}
				}
				else { // 1. gefundenes Rechteck
					rechteckgefunden=1;
					zusammenhang[i]=1; // Teilprobleme in denen Rechtecke auftreten
					a2 = y*(i+1)/size; // Anfang des nächsten Problems
					a1 = y* i   /size; // Anfang des aktuellen Problems
					if (re[i].xstart  == re[i+1].xstart &&		// linke Seite
						re[i].xende   == re[i+1].xende  &&		// rechte Seite
						re[i].yende + a1 + 1 == re[i+1].ystart + a2) {	// Ende i (oberes Rechteck) == Anfang i+1 (unteres Rechteck)
						zusammenhang[i+1]=1; // rechteck hängt zusammen
					}
					else {
						erwarterechteck=0; // wird bei nächster Iteration sofort den Fehler erkennen
					}
				}
			}
		}

		// wenn bis jetzt kein Fehler gefunden wurde, sind keine vorhanden
		int summe=0;
		for (int i=0; i<size; i++) {
			summe+=zusammenhang[i];
		}
		if (!summe) {
			if (debug) printf("Es ist kein Rechteck vorhanden.\n");
		}
		else {
	 		if (debug) printf("Das Rechteck ist zusammenhängend!\n");
		}

		// Zeitunterschiede herausrechnen
		uninit(0, size, x, rechtecke, startt, start2t, clk_id);
	}

	// Slave
	else {
		// sicherstellen, dass der Slave eine Daseinberechtigung hat
		if (rank<size) {
			int	x,y;
			struct rechteck re = {-1,-1,-1,-1}; // nötig um zu erkennen, ob das Rechteck bearbeitet wurde

			// MPI_Recv(void* data, int count, MPI_Datatype datatype, int source, int tag, MPI_Comm communicator, MPI_Status* status)
			MPI_Recv(&y, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, MPI_STATUS_IGNORE); // empfange vertikale Größe des Teilproblems
			MPI_Recv(&x, 1, MPI_INT, 0, 1, MPI_COMM_WORLD, MPI_STATUS_IGNORE); // empfange horizontale Problemgröße

			char * feld = (char*) malloc(x*y*sizeof(char));
			if (feld == NULL) return 42;

			MPI_Status stat;
			MPI_Recv(feld, x*y, MPI_CHAR, 0, 2, MPI_COMM_WORLD, &stat);

			// wenn Fehler zurück kommt, ist Rechteck nicht zusammenhängend
			re.fehler=findeRechteck(feld, x, y, schwarz, &re);

			MPI_Send(&re,  1, rechteck_type, 0, 0, MPI_COMM_WORLD);
		}
		else {
			if (mode == 1) fclose(f);
		}
	}

	MPI_Type_free(&rechteck_type);
	MPI_Finalize();
	return 0;
}

void uninit(int code, int size, int x, int rechtecke, struct timespec startt, struct timespec start2t, clockid_t clk_id) {
	if (code != 0) exit(code);

	long long t=0; // Zeit in ns
	struct timespec stopt;

	// Zeitunterschiede herausrechnen
	clock_gettime(clk_id, &stopt);
	t=((long long)start2t.tv_sec*1000000000+start2t.tv_nsec) - ((long long)startt.tv_sec*1000000000+startt.tv_nsec);
	t=((long long)stopt.tv_sec*1000000000+(long long)stopt.tv_nsec) - ((long long)startt.tv_sec*1000000000+(long long)startt.tv_nsec) - 2*t;
	printf("%d\t%d\t%d\t%lld\n", size, x, rechtecke, t);

    MPI_Finalize();
	exit(0);
}


int findeRechteck(char * arr, int x , int y, char schwarz, struct rechteck * re) {
	int	k=0, l=0,
		i=0, j=0,
		m=0, n=0,
		o=0, p=0; // Zwischenspeicher für Koordinaten
	int	rechteckgefunden=0;
	char	feld[x][y];

	// kopiere Feld für einfachere Handhabung
	for (i=0; i<x; i++) {
		for (j=0; j<y; j++) {
			feld[i][j]=arr[i+j*x];
		}
	}

	// gehe Feld durch, bis eine Schwärzung gefunden wird
	for (i=0; i<x; i++) {
		for (j=0; j<y; j++) {
			// Rechteckanfang gefunden
			if (feld[i][j] == schwarz) {
				if (rechteckgefunden==0){
					rechteckgefunden=1;
				}
				else {
					// prüfe ob Schwärzung innerhalb des gefundenen Rechteck ist
					if ( i >=k && i <= o && j >=l && j <= p ) continue; // Schwärzung wurde schon geprüft
					else return 1; // !zusammenhängend
				}

				// speichere Anfangskoordinaten
				re->xstart=k=i;
				re->ystart=l=j;

				// gehe so weit wie möglich nach rechts
				for (m=i; m<x; m++) {
					if (feld[m][j] != schwarz) break;
					o=m;
				}

				// gehe so weit wie möglich nach unten
				for (n=j; n<y; n++) {
					if (feld[i][n] != schwarz) break;
					p=n;
				}

				// speichere Endkoordinaten
				re->xende=o;
				re->yende=p;

				// o und p enthalten jetzt die weitesten Schwärzungen
				// prüfe ob alles inerhalb schwarz ist
				for (m=i; m<o+1; m++) {
					for (n=j; n<p+1; n++) {
						if (feld[m][n] != schwarz) { // Feld hat Löcher oder Lücken
							return 1; // !zusammenhängend
						}
					}
				}
			}
		}
	}
	return 0; // rechteck ist zusammenhängend
}

/* read_pbm_header:
 * Read the header contents of a PBM (Portable Binary Map) file.
 * An ASCII PBM image file follows the format:
 * P1
 * <X> <Y>
 * <I1> <I2> ... <IMAX>
 * A binary PBM image file uses P4 instead of P1 and
 * the data values are represented in binary.
 * NOTE1: Comment lines start with '#'.
 * NOTE2: < > denote integer values (in decimal).
 */ void read_pbm_header(FILE *f, int *img_xdim, int *img_ydim, int *is_ascii) {
  int flag=0;
  int x_val, y_val;
  unsigned int i;
  char magic[MAXLINE];
  char line[MAXLINE];
  int count=0;

  /* Read the PBM file header. */
  while (fgets(line, MAXLINE, f) != NULL) {
    flag = 0;
    for (i = 0; i < strlen(line); i++) {
      if (isgraph(line[i])) {
        if ((line[i] == '#') && (flag == 0)) {
          flag = 1;
        }
      }
    }
    if (flag == 0) {
      if (count == 0) {
        count += sscanf(line, "%s %d %d", magic, &x_val, &y_val);
      } else if (count == 1) {
        count += sscanf(line, "%d %d", &x_val, &y_val);
      } else if (count == 2) {
        count += sscanf(line, "%d", &y_val);
      }
    }
    if (count == 3) {
      break;
    }
  }
  if (strcmp(magic, "P1") == 0) {
    *is_ascii = 1;
  } else if (strcmp(magic, "P4") == 0) {
    *is_ascii = 0;
  } else {
    fprintf(stderr, "Error: Input file not in PBM format!\n");
    exit(100);
  }

//  fprintf(stderr, "Info: magic=%s, x_val=%d, y_val=%d\n", magic, x_val, y_val);
  *img_xdim = x_val;
  *img_ydim = y_val;
}

/* read_pbm_data:
 * Read the data contents of a PBM (portable bit map) file.
 */
long long read_pbm_data(FILE *f, char *img_in, int ascii) {
  long long i=0;
  int c;
  while ((c = fgetc(f)) != EOF) {
    if (isgraph(c))
      img_in[i++] = c == '1' ? '1' : '0';
  }
  return i;
  fclose(f);
}


void printmode (int i) {
	if (i==0) {
		printf("Benutzung: mpirun -hostfile <pfad/hostsdatei> re 0 <debugging> <Feldgröße> <Anzahl Rechteecke innerhalb>\n");
		printf("Beispiel: Erstelle Feld der Größe 20000 mit 4 schwarzen Rechtecken und schalte debugging an.\n");
		printf("\tmpirun -hostfile ../myhosts re 0 1 20000 4\n\n");
		printf("Beispiel: Erstelle Feld der Größe 1000, fülle alles schwarz und schalte debugging an.\n");
		printf("\tmpirun -hostfile ../myhosts re 0 1 1000 -1\n\n");
		printf("Beispiel: Erstelle Feld der Größe 5000 mit keinem Rechteck und schalte debugging aus.\n");
		printf("\tmpirun -hostfile ../myhosts re 0 0 20000 0\n\n");
	}
	if (i==1) {
		printf("\nBenutzung: mpirun -hostfile <pfad/hostsdatei> re 1 <debugging> <Dateiname>\n");
		printf("Beispiel: Lese Datei beispiel.pbm ein und schalte debugging an.\n");
		printf("\tmpirun -hostfile ../myhosts re 1 1 beispiel.pbm\n");
	}
}
