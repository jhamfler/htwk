#!/bin/bash
ulimit -s unlimited # stackgröße erhöhen, damit keine segmentation faults auftreten - funktioniert nur lokal

prog=re	# Programmname
modus=0	# generiere die Rechtecke automatisch
debug=0	# nichts ausgeben, nur die Zeitmessung

prozessoren=180 # maximale zu nutzende Anzahl an Knoten
pfaktor=4 # 4 Kerne pro Knoten

messpfad=mess/
messreihen=30
rechtecke=5 # -1 bis 5 = 7
n=5000 # feste Problemgröße

# finde alle verfügbaren hosts
pool=imunix
#z423=$(./simson_alive_test |grep -v OFF|grep " on"|grep -o "simson..")
#lebend=$(./$pool"_alive_test" |grep -v OFF|grep " on"|grep -oi "$pool..")

declare -a hosts=(
"imunix03 slots=4"
"imunix05 slots=4"
"imunix06 slots=4"
"imunix07 slots=4"
"imunix08 slots=4"
"imunix09 slots=4"
"imunix10 slots=4"
"imunix11 slots=4"
"imunix12 slots=4"
"imunix13 slots=4"
"imunix14 slots=4"
"imunix15 slots=4"
"imunix16 slots=4"
"imunix17 slots=4"
"imunix18 slots=4"
"imunix19 slots=4"
"imunix21 slots=4"
)

i=0
for h in ${hosts[@]}
do
	echo ${hosts[$i]}
	i=$(($i+1))
done

# nur nachts möglich, da sonst zu viele andere Nutzer vorhanden sind
#declare -a hosts=()
#i=0
#for h in ${lebend[@]}
#do
#	hosts[$i]+="$h"" slots=$pfaktor"
#	echo ${hosts[$i]}
#	i=$(($i+1))
#done

# zu wenig Knoten verfügbar
if (( $prozessoren > $i ))
then
	prozessoren=$i
fi

mkdir -p $messpfad # sicherstellen dass messpfad existiert
echo -n "" > $messpfad"hosts" # leeren
for p in $(seq 1 $prozessoren)
do
	echo ${hosts[$p]} >> $messpfad"hosts"	# fülle nach jeder messreihe die hosts auf

	for i in $(seq 1 $messreihen)
	do
		for rechteckanzahl in $(seq -1 $rechtecke)
		do
			echo mpirun -hostfile $messpfad"hosts" $prog $modus $debug $n $rechteckanzahl  $messpfad"$n"
			mpirun -hostfile $messpfad"hosts" $prog $modus $debug $n $rechteckanzahl >> $messpfad"$n"
		done
	done

done
