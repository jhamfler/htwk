# Message-Passing-Projekt zum finden größter zusammenhängender Rechtecke in einem Bild/Feld

Die Dokumentation ist unter [Dokumentation](Hamfler,Johannes_p1/Hamfler,Johannes_p1.pdf) zu finden. Es wird empfohlen, sich diese nicht im Browser anzusehen.

Der Quelltext liegt unter [Quelltext](Hamfler,Johannes_p1/projekt). Die Datei `re.c` enthält das Hauptprogramm. MPI ist voraussetzung für die Ausführung.