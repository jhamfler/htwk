# Evolutionärer Algorithmus zur Lösung des JobShop-Problems

Die Dokumentation ist unter [Dokumentation](latex/Dokumentation/EA_Hamfler,Johannes.pdf) zu finden. Es wird empfohlen, die Dokumentation nicht im Browser zu betrachten, da Seiten vertauscht sein können und die Anzahl an Messpunkten die Anzeige verlangsamt.

Der Quelltext ist unter [Quelltext](src/) zu finden. Der Unterordner `jsp` enthält den evolutionären Algorithmus, `generatejsp` den Quelltext zur Generierung eines Problems und `benchmark` die Skripte zum Sammeln von Messdaten.