extern crate clap; // CLI args
extern crate rand;

use clap::{Arg, App}; // CLI args
use rand::{thread_rng, Rng};
use std::fs::File;
use std::io::prelude::*;
use std::io::BufWriter;


fn ist_ganzzahl(string : String) -> Result<(), String> {
    match string.parse::<i64>() {
        Ok(_) => Ok(()),
        Err(e) => Err(format!(" Es wurde kein ganzzahliger Typ übergeben. {}", e).to_string()),
    }
}

fn main() {
    let matches = App::new("Job Shop with Penalties")
        .version("0.1")
        .author("Johannes Hamfler <johannes.hamfler@stud.htwk-leipzig.de>")
        .about("Generiert eine Instanz für das Job Shop Problem.")
        .arg(Arg::with_name("Ausgabedatei")
            .help("Ausgabedatei angeben")
            .required(true))
        .arg(Arg::with_name("Zeiten")
             .short("z")
             .long("zeit")
             .help("legt die maximale Zeit pro Job fest")
             .required(false)
             .takes_value(true)
             .validator(ist_ganzzahl)
             .default_value("1000"))
        .arg(Arg::with_name("Fristen")
             .short("f")
             .long("frist")
             .help("legt den Faktor für die Frist pro Job fest (Anzahl Jobs * Faktor)")
             .required(false)
             .takes_value(true)
             .validator(ist_ganzzahl)
             .default_value("500"))
        .arg(Arg::with_name("Strafen")
             .short("s")
             .long("strafe")
             .help("legt die maximale Strafe pro Job fest")
             .required(false)
             .takes_value(true)
             .validator(ist_ganzzahl)
             .default_value("1000"))
        .arg(Arg::with_name("Anzahl")
             .short("a")
             .long("anzahl")
             .help("legt die Anzahl an Jobs fest")
             .required(false)
             .takes_value(true)
             .validator(ist_ganzzahl)
             .default_value("100"))
        .get_matches();

    let ausgabedatei = matches.value_of("Ausgabedatei").unwrap();
    let anzahl = matches.value_of("Anzahl").unwrap().parse::<u64>().unwrap();
    let zeit = matches.value_of("Zeiten").unwrap().parse::<u64>().unwrap();
    let frist = matches.value_of("Fristen").unwrap().parse::<u64>().unwrap();
    let strafe = matches.value_of("Strafen").unwrap().parse::<u64>().unwrap();

    let mut zeiten  :Vec<u64> = Vec::new();
    let mut fristen :Vec<u64> = Vec::new();
    let mut strafen :Vec<u64> = Vec::new();
    let mut rng = rand::thread_rng();

    // generiere Zeiten
    for i in 0..anzahl {
        zeiten.push(rng.gen_range(0, zeit));
        fristen.push(rng.gen_range(0, anzahl*frist));
        strafen.push(rng.gen_range(0, strafe));
    }

    let f = File::create(ausgabedatei).expect("kein FS Zugriff möglich");
    let mut writer = BufWriter::new(f);
    for z in zeiten {
        write!(writer, "{} ", z);
    }
    write!(writer, "\n");
    for f in fristen {
        write!(writer, "{} ", f);
    }
    write!(writer, "\n");
    for s in strafen {
        write!(writer, "{} ", s);
    }
}
