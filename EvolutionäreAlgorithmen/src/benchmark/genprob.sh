#!/bin/bash

# anzahl zeiten fristfaktor strafen
# frist = anzahl * fristfaktor
# fristfaktor = 100 * fristfaktor -> Anteil von Zeiten
#declare -a anzahl=("10" "100")
declare -a anzahl=("10")
declare -a zeiten=("1000") # höhe der zeiten sollte bei großen werten !rolle spielen
declare -a fristfaktor=("4") # anteil der frist von der max jobanzahl*maxzeit
declare -a strafen=("1000") # höhe der strafen sollte bei großen werten !rolle spielen

z=1000
s=1000

problemort=probleme/
mkdir -p $problemort

for a in ${anzahl[@]}
do
    for f in ${fristfaktor[@]}
    do
        ../generatejsp/target/release/generatejsp $problemort$a.$f -z $z -f $((zeiten / f))  -s $s -a $a
		sed -i 's/ *$//' $problemort$a.$f
    done
done

