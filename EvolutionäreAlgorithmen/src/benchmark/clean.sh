#!/bin/bash

cd messwerte || exit
for file in *
do
	rm "$file"
done

cd ..
cd probleme || exit
for file in *
do
	rm "$file"
done

cd ..
cd graphen || exit
for file in *
do
    rm "$file"
done

cd ..
cd r || exit
for file in *
do
    rm "$file"
done

