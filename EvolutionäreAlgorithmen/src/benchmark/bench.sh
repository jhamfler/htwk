#!/bin/bash
INFO=1
GENPROB=0
MESSUNG=1
RSKRIPT=1
PDF=1
GENDEC=0
wd="$(pwd)"

#problemgröße.fristfaktor.populationsgröße.mutation.rekombination.selektion.messung

# anzahl zeiten fristfaktor strafen
# frist = anzahl * fristfaktor
# fristfaktor = 100 * fristfaktor -> Anteil von Zeiten
declare -a anzahl=("100")
#declare -a anzahl=("10" "50" "100" "1000") # Problemgröße / Anzahl Aufträge
declare -a zeiten=("1000") # höhe der zeiten sollte bei großen werten !rolle spielen
declare -a strafen=("1000") # höhe der strafen sollte bei großen werten !rolle spielen
#declare -a fristfaktor=("4" "8" "16") # anteil der frist von der max jobanzahl*maxzeit
declare -a fristfaktor=("8") # anteil der frist von der max jobanzahl*maxzeit
#declare -a rekombinationen=("zwei-punkt-crossover" "ein-punkt-crossover")
declare -a rekombinationen=("ordnung")
#declare -a mutationen=("vertausche" "invertiere" "verschiebe" "mische")
declare -a mutationen=("vertausche")
#declare -a selektionsmethoden=("stochastisch" "fitnessproportional")
declare -a selektionsmethoden=("stochastisch")
#declare -a popsize=("2" "5" "10" "100" "500")
#declare -a popsize=("5" "10" "20" "50" "100" "200")
declare -a popsize=("10")
k=0
z=1000
s=1000
generationen=10000
anzahlmessungen=400

problemort=probleme/
realproblemort=$problemort
messzielpfad=messwerte/
mittelwertpfad=mittelwerte/
graphort=graphen/
rort=r/
mkdir -p $problemort
mkdir -p $messzielpfad
mkdir -p $mittelwertpfad
mkdir -p $graphort
mkdir -p $rort

if (( $GENPROB > 0 ))
then
	echo "generiere Problem"
	for a in ${anzahl[@]}
	do
	    for f in ${fristfaktor[@]}
	    do
	        ../generatejsp/target/release/generatejsp $problemort$a.$f -z $z -f $((zeiten / f)) -s $s -a $a
	    done
	done
fi

SAVEIFS=$IFS
IFS=$(echo -en "\n\b")

if (( $MESSUNG > 0 ))
then
if (( $INFO > 0 )); then echo "starte Messung"; fi
for a in ${anzahl[@]}
do
    for f in ${fristfaktor[@]}
    do
    for p in ${popsize[@]}
    do
        for m in ${mutationen[@]}
        do
            for r in ${rekombinationen[@]}
            do
                for s in ${selektionsmethoden[@]}
                do
					if (( $MESSUNG > 0 ))
					then
                    	echo "Bewertung    Generation    Durchschnitt" > "$messzielpfad$a.$f.$p.$m.$r.$s"
                    	for i in $(seq 1 $anzahlmessungen)
                    	do
							g=$generationen
							if (( $GENDEC > 0 )); then g=$((generationen / $p)); fi
	                	    if (( $INFO > 0 )); then echo "Messung $i"; fi
                    	    echo ../jsp/target/release/jsp "$realproblemort$a.$f" -g $g -K $k -p $p -m $m -r $r -s $s -cccc -b 1 \>\> "$messzielpfad$a.$f.$p.$m.$r.$s"
                    	    ../jsp/target/release/jsp "$realproblemort$a.$f" -g $g -K $k -p $p -m $m -r $r -s $s -cccc -b 1 >> "$messzielpfad$a.$f.$p.$m.$r.$s"
                    	done
					fi
                    if (( $RSKRIPT > 0 ))
					then
						if (( $INFO > 0 )); then echo "R-Skript generieren"; fi
						plotdatei=r/"$a.$f.$p.$m.$r.$s"
						# r skript vorbereiten
					    echo 'messung = read.table("'$wd/messwerte/$a.$f.$p.$m.$r.$s'", header = TRUE)' >> $plotdatei
					    echo "a=aggregate(Bewertung ~ Generation, messung, mean)" >> $plotdatei
	    				echo 'lo <- loess(a$Bewertung~a$Generation)' >> $plotdatei
					    echo 'pdf("'$wd/$graphort$a.$f.$p.$m.$r.$s'.pdf")' >> $plotdatei
					    echo 'plot(a$Bewertung ~ a$Generation, a, main = "'"Aufträge: $a Populationsgröße: $p"'", sub="'Mutation: $m Rekombination: $r Selektion: $s'", xlab = "Generation", ylab = "Bewertung", pch=".")' >> $plotdatei
	    				echo "lines(smooth.spline(lo), lwd=2)" >> $plotdatei
	   					echo 'dev.off()' >> $plotdatei
					fi
					if (( $PDF > 0 ))
                    then
						if (( $INFO > 0 )); then echo "PDF generieren"; fi
						R --no-save < $plotdatei
					fi
				done
            done
        done
    done
    done
done
fi

IFS=$SAVEIFS
