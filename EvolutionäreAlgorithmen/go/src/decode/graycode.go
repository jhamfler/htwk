package decode

import (
	"qfunc"
	//"fmt"
)

type GrayCodeDec struct  {
	dim int
	dec *StdBinDec
}
//var lb float64
//var ub float64

func (p *GrayCodeDec) Init (dim int, lbound, ubound float64, qf qfunc.QFunctionRealVal) *GrayCodeDec {
	p.dec = new(StdBinDec).Init(dim,lbound,ubound,qf)
	p.dim = 2*dim
	return p
}

func (p *GrayCodeDec) GetDim () int {
	return p.dim
}

func (p *GrayCodeDec) IsBetter (new, old float64) bool {
	return p.dec.IsBetter(new, old)
}

func (p *GrayCodeDec) IsOptimumReached (val float64) bool {
	return p.dec.IsOptimumReached(val)
}

func (p *GrayCodeDec) Evaluate (genotype []byte) float64 {
	return p.dec.Evaluate(p.grayToStdbin(genotype))
}

func (p *GrayCodeDec) PrintPhenotype (genotype []byte) {
	p.dec.PrintPhenotype(p.grayToStdbin(genotype))
}

func (p *GrayCodeDec) grayToStdbin (genotype []byte) []byte {
	var decoded []byte = make([]byte, p.dim)
	for i:=0;i<len(genotype)-1;i+=2 {
		decoded[i] = genotype[i]
		decoded[i+1] = genotype[i+1]
		p.transform(decoded[i:i+2])
	}
	return decoded
}

func (p *GrayCodeDec) transform (onenumber []byte) {
	//fmt.Printf("%08b",onenumber[0])
	//fmt.Printf("%08b",onenumber[1])
	//fmt.Print(" - ")
	var result uint16 = uint16( 256*(uint16(onenumber[0]))+uint16(onenumber[1]))
	result ^= (result >> 8)
	result ^= (result >> 4)
	result ^= (result >> 2)
	result ^= (result >> 1)
	onenumber[0] = byte(result >> 8)
	onenumber[1] = byte( result - ( uint16(onenumber[0]) << 8) )
	//fmt.Printf("%08b",onenumber[0])
	//fmt.Printf("%08b",onenumber[1])
	//fmt.Println()
}
