package decode

import (
	"qfunc"
	//"fmt"
)

type StdBinDec struct  {
	dim int
	lb float64
	ub float64
	qf qfunc.QFunctionRealVal
}
//var lb float64
//var ub float64

func (p *StdBinDec) Init (dim int, lbound, ubound float64, qf qfunc.QFunctionRealVal) *StdBinDec {
	p.lb = lbound
	p.ub = ubound
	p.dim = 2*dim
	p.qf = qf
	return p
}

func (p *StdBinDec) GetDim () int {
	return p.dim
}

func (p *StdBinDec) IsBetter (new, old float64) bool {
	return p.qf.IsBetter(new, old)
}

func (p *StdBinDec) IsOptimumReached (val float64) bool {
	return p.qf.IsOptimumReached(val)
}

func (p *StdBinDec) Evaluate (genotype []byte) float64 {
	return p.qf.Evaluate(p.stdBinDecode(genotype))
}

func (p *StdBinDec) PrintPhenotype (genotype []byte) {
	p.qf.PrintPhenotype(p.stdBinDecode(genotype))
}

func (p *StdBinDec) stdBinDecode (genotype []byte) []float64 {
	var values []float64 = make([]float64, len(genotype)/2)
	for i:=0;i<len(genotype)-1;i+=2 {
		values[i>>1] = p.decode(genotype[i:i+2])
	}
	//fmt.Print(values[0])
	//fmt.Print(" ")
	//fmt.Println(values[1])
	return values
}

func (p *StdBinDec) decode (onenumber []byte) float64 {
	var number uint16 = uint16( 256*(uint16(onenumber[0]))+uint16(onenumber[1]))
	return p.lb + ( float64(number)/65536.0 ) * (p.ub - p.lb)
}
