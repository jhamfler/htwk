package main

import (
	"fmt"
	"time"
	"flag"
	"math/rand"
	"qfunc"
	"decode"
)

var tmax = flag.Int("tmax", 500, "maximal number of generations")
var dim = flag.Int("dim", 3, "dimension (number of bytes)")
var qfname = flag.String("problem","onemax","name of the quality function to be optimized - onemax, knapsack, hiff, wsphere,rastrigin,rosenbrock")
var mutationname = flag.String("mutation","onebit","mutation operator - onebit,probbitflip")
var mutrate = flag.Float64("mutrate",-1.0,"mutation probability -1=1/laenge")
var compact = flag.Bool("compact",false,"compact output, improvement only")
var gray = flag.Bool("gray",false,"use Gray code instead of standard binary")
var ksackdec = flag.Bool("knapsackdec",false,"use decoder in knapsack quality function")

type individual struct {
	genotype []byte
	quality float64 
}


type mutationOp func(ind *individual) *individual

var rg = rand.New(rand.NewSource(99))

func NewIndividual() *individual {
	var ind individual
	var geno = make([]byte,*dim)
	ind.genotype = geno
	for i:=0; i<*dim; i++ {
		geno[i] = byte(rg.Intn(255))
	}
	ind.quality = 0.0
	return &ind
}

func main() {
	flag.Parse()
	rg.Seed(time.Now().UnixNano())
	var qf qfunc.QFunctionBinary
	switch *qfname {
	case "onemax":
		qf = new(qfunc.Onemax).Init(*dim)
	case "knapsack":
		var knprob *qfunc.Knapsack = new(qfunc.Knapsack).Init(*dim,*ksackdec)
		*dim = knprob.GetDim()
		qf = knprob
	case "hiff":
		qf = new(qfunc.Hiff).Init(*dim)
	case "wsphere":
		realObjf := new(qfunc.WeightedSphere).Init(*dim)
		qf = useDecoder(-5.12,5.12,realObjf)
	case "rastrigin":
		realObjf := new(qfunc.Rastrigin).Init(*dim)
		qf = useDecoder(-5.12,5.12,realObjf)
	case "rosenbrock":
		realObjf := new(qfunc.Rosenbrock).Init(*dim)
		qf = useDecoder(-2.048,2.048,realObjf)
	default:
		panic("unrecognized quality function")
	}
	var mutate mutationOp 
	switch *mutationname {
	case "onebit":
		mutate = mutateOnebit
	case "probbitflip":
		mutate = mutateBitflip
		if (*mutrate<0.0) {
			*mutrate = float64(0.125)/float64(*dim)
		}
	default:
         panic("unrecognized mutation operator")
	}
	best, evaluations := hillclimb(qf,mutate)
	printInd(best)
	fmt.Print("number of evaluations: ")
	fmt.Println(evaluations)
	qf.PrintPhenotype(best.genotype)
}

func  useDecoder (lb, ub float64, of qfunc.QFunctionRealVal) qfunc.QFunctionBinary {
	var qf qfunc.QFunctionBinary
	if (*gray) {
		decoder := new(decode.GrayCodeDec).Init(*dim,lb,ub,of)
		*dim = decoder.GetDim()
		qf = decoder
	} else {
		decoder := new(decode.StdBinDec).Init(*dim,lb,ub,of)
		*dim = decoder.GetDim()
		qf = decoder
	}
	return qf
}

func printInd(ind *individual) {
	for _, g := range ind.genotype {
		fmt.Printf("%08b",g)
	}
	fmt.Print(" - ")
	fmt.Println(ind.quality)
}

//func hillclimb(evaluate Assessment, mutate mutationOp) (*individual, int) {
func hillclimb(qf qfunc.QFunctionBinary, mutate mutationOp) (*individual, int) {
	t := 0
	ind := NewIndividual()
	ind.quality = qf.Evaluate(ind.genotype)
	printInd(ind)
	for !qf.IsOptimumReached(ind.quality) &&  t < *tmax {
		offspring := mutate(ind)
		offspring.quality = qf.Evaluate(offspring.genotype)
		isImprovement := false
		if qf.IsBetter(offspring.quality, ind.quality) {
			ind = offspring
			isImprovement = true
		}
		t++
		if (*compact) {
			if (isImprovement) {
				fmt.Print(t)
				fmt.Print(" ")
				printInd(ind)
			}
		} else {
			printInd(ind)
		}
	}
	return ind, t
}

func mutateOnebit (ind *individual) *individual {
	var offspring *individual = new(individual)
	var geno = make([]byte,len(ind.genotype))
	offspring.genotype = geno
	for i:=0; i<len(geno); i++ {
		geno[i] = ind.genotype[i]
	}
	index := rg.Intn(8*len(geno))
	pos := uint(index%8)
	geno[index/8] ^= byte(1) << pos
	return offspring
}

func mutateBitflip (ind *individual) *individual {
	var offspring *individual = new(individual)
	var geno = make([]byte,len(ind.genotype))
	offspring.genotype = geno
	for i:=0; i<len(geno); i++ {
		geno[i] = ind.genotype[i]
		var mutmask byte = byte(0)
		for k:=0;k<8;k++ {
			mutmask = mutmask << 1
			if (rg.Float64()<*mutrate) {
				mutmask  ^= byte(1)
			}
		}
		geno[i] ^= mutmask
	}
	return offspring
}
