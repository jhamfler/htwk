package main

import (
	"fmt"
	"time"
	"flag"
	"math/rand"
	"qfunc"
	"decode"
)

var tmax = flag.Int("tmax", 500, "maximal number of generations")
var dim = flag.Int("dim", 3, "dimension (number of bytes)")
var qfname = flag.String("problem","onemax","name of the quality function to be optimized - onemax, knapsack, hiff, wspheremax,rastriginmax")
var mutationname = flag.String("mutation","onebit","mutation operator - onebit,probbitflip")
var mutrate = flag.Float64("mutrate",-1.0,"mutation probability -1=1/laenge")
var recombinationname = flag.String("recombination","onepoint","rekombination operator - onepoint,twopoint,uniform")
var recrate = flag.Float64("recrate",0.8,"recombination rate")
var popsize = flag.Int("popsize", 50, "population size")
var selectionname = flag.String("selection","fitprop","selection operator - fitprop,tournament,fitpropscale")
var tournamentsize = flag.Int("tournamentsize",3,"number of individuals for tournament selection")
var compact = flag.Bool("compact",false,"compact output, improvement only")
var gray = flag.Bool("gray",false,"use Gray code instead of standard binary")
var ksackdec = flag.Bool("knapsackdec",false,"use decoder in knapsack quality function")

type individual struct {
	genotype []byte
	quality float64 
}

type population struct {
	indi [](*individual)
	prob []float64
} 

type mutationOp func(ind *individual) *individual
type recombinationOp func(a, b *individual) (*individual,*individual)
type selectionOp func(pop *population) *individual

var rg = rand.New(rand.NewSource(99))

func NewIndividual() *individual {
	var ind individual
	var geno = make([]byte,*dim)
	ind.genotype = geno
	for i:=0; i<*dim; i++ {
		geno[i] = byte(rg.Intn(255))
	}
	ind.quality = 0.0
	return &ind
}

func NewPopulation() *population {
	var pop population
	var indi = make([](*individual),*popsize)
	pop.indi = indi
	pop.prob = make([]float64, *popsize)
	for i:=0; i<*popsize; i++ {
		indi[i] = NewIndividual()
	}
	return &pop
}

func (p *population) eval(qf qfunc.QFunctionBinary) {
	for i:=0; i<*popsize; i++ {
		p.indi[i].quality = qf.Evaluate(p.indi[i].genotype)
	}
}

func (p *population) getBest(qf qfunc.QFunctionBinary) *individual {
	bestind := p.indi[0]
	for i:=1; i<*popsize; i++ {
		if qf.IsBetter(p.indi[i].quality, bestind.quality) {
			bestind = p.indi[i]
		}
	}
	return bestind
}

func main() {
	flag.Parse()
	rg.Seed(time.Now().UnixNano())
	var qf qfunc.QFunctionBinary
	switch *qfname {
	case "onemax":
		qf = new(qfunc.Onemax).Init(*dim)
	case "knapsack":
		var knprob *qfunc.Knapsack = new(qfunc.Knapsack).Init(*dim,*ksackdec)
		*dim = knprob.GetDim()
		qf = knprob
	case "hiff":
		qf = new(qfunc.Hiff).Init(*dim)
	case "wspheremax":
		realObjf := new(qfunc.WeightedSphereMax).Init(*dim)
		qf = useDecoder(-5.12,5.12,realObjf)
	case "rastriginmax":
		realObjf := new(qfunc.RastriginMax).Init(*dim)
		qf = useDecoder(-5.12,5.12,realObjf)
	default:
		panic("unrecognized quality function")
	}
	var mutate mutationOp 
	switch *mutationname {
	case "onebit":
		mutate = mutateOnebit
	case "probbitflip":
		mutate = mutateBitflip
		if (*mutrate<0.0) {
			*mutrate = float64(0.125)/float64(*dim)
		}
	default:
         panic("unrecognized mutation operator")
	}
	var recombine recombinationOp 
	switch *recombinationname {
	case "onepoint":
		recombine = recombineOnepoint
	case "twopoint":
		recombine = recombineTwopoint
	case "uniform":
		recombine = recombineUniform
	default:
         panic("unrecognized recombination operator")
	}
	var selectOne selectionOp 
	switch *selectionname {
	case "fitprop":
	case "fitpropscale":
		selectOne = selectFitprop
	case "tournament":
		selectOne = selectTournament
	default:
         panic("unrecognized selection operator")
	}
	best, evaluations := geneticalg(qf,mutate,recombine,selectOne)
	printInd(best)
	fmt.Print("number of evaluations: ")
	fmt.Println(evaluations)
	qf.PrintPhenotype(best.genotype)
}

func  useDecoder (lb, ub float64, of qfunc.QFunctionRealVal) qfunc.QFunctionBinary {
	var qf qfunc.QFunctionBinary
	if (*gray) {
		decoder := new(decode.GrayCodeDec).Init(*dim,lb,ub,of)
		*dim = decoder.GetDim()
		qf = decoder
	} else {
		decoder := new(decode.StdBinDec).Init(*dim,lb,ub,of)
		*dim = decoder.GetDim()
		qf = decoder
	}
	return qf
}

func printInd(ind *individual) {
	for _, g := range ind.genotype {
		fmt.Printf("%08b",g)
	}
	fmt.Print(" - ")
	fmt.Println(ind.quality)
}

//func hillclimb(evaluate Assessment, mutate mutationOp) (*individual, int) {
func geneticalg(qf qfunc.QFunctionBinary, mutate mutationOp, recombine recombinationOp, selectOne selectionOp) (*individual, int) {
	t := 0
	pop := NewPopulation()
	pop.eval(qf)
	ind := pop.getBest(qf)
	printInd(ind)
	for !qf.IsOptimumReached(ind.quality) &&  t < *tmax {
		var offspring population
		offspring.indi = make([](*individual),*popsize)
		if (*selectionname == "fitprop" || *selectionname == "fitpropscale") {
			pop.computeSelectProbabilities()
		}
		for i:=0; i<*popsize; i=i+2 {
			a := selectOne(pop)
			b := selectOne(pop)
			c,d := recombine(a,b)
			pop.indi[i] = mutate(c)
			pop.indi[i+1] = mutate(d)
		}
		pop.eval(qf)
		newbest := pop.getBest(qf)
		isImprovement := qf.IsBetter(newbest.quality, ind.quality)
		ind = newbest
		t++
		if (*compact) {
			if (isImprovement) {
				fmt.Print(t)
				fmt.Print(" ")
				printInd(ind)
			}
		} else {
			printInd(ind)
		}
	}
	return ind, t
}

func mutateOnebit (ind *individual) *individual {
	var offspring *individual = new(individual)
	var geno = make([]byte,len(ind.genotype))
	offspring.genotype = geno
	for i:=0; i<len(geno); i++ {
		geno[i] = ind.genotype[i]
	}
	index := rg.Intn(8*len(geno))
	pos := uint(index%8)
	geno[index/8] ^= byte(1) << pos
	return offspring
}

func mutateBitflip (ind *individual) *individual {
	var offspring *individual = new(individual)
	var geno = make([]byte,len(ind.genotype))
	offspring.genotype = geno
	for i:=0; i<len(geno); i++ {
		geno[i] = ind.genotype[i]
		var mutmask byte = byte(0)
		for k:=0;k<8;k++ {
			mutmask = mutmask << 1
			if (rg.Float64()<*mutrate) {
				mutmask  ^= byte(1)
			}
		}
		geno[i] ^= mutmask
	}
	return offspring
}

var maskOnepointA []byte = []byte{ byte(0), byte(128), byte(192), byte(224), byte(240), byte(248), byte(252), byte(254) }
var maskOnepointB []byte = []byte{ byte(255), byte(127), byte(63), byte(31), byte(15), byte(7), byte(3), byte(1) }

func recombineOnepoint (a,b *individual) (*individual,*individual) {
	if (rg.Float64()<*recrate) {
		var c *individual = new(individual)
		var d *individual = new(individual)
		c.genotype = make([]byte,len(a.genotype))
		d.genotype = make([]byte,len(a.genotype))
		index := rg.Intn(8*len(a.genotype)-1)
		bytenum := index/8
		pos := uint(index%8)
		for i:=0; i<bytenum; i++ {
			c.genotype[i] = a.genotype[i]
			d.genotype[i] = b.genotype[i]
		}
		for i:=bytenum+1; i<len(a.genotype); i++ {
			c.genotype[i] = b.genotype[i]
			d.genotype[i] = a.genotype[i]
		}
		c.genotype[bytenum],d.genotype[bytenum] = 
			onepointInOneByte(a.genotype[bytenum],b.genotype[bytenum],pos)
		return c,d
	} else {
		return a,b
	}
}

func onepointInOneByte(a,b byte, pos uint) (byte,byte) {
	c :=  (maskOnepointA[pos] & a) | (maskOnepointB[pos] & b)
	d :=  (maskOnepointB[pos] & a) | (maskOnepointA[pos] & b)
	return c,d
}

func recombineUniform (a,b *individual) (*individual,*individual) {
	if (rg.Float64()<*recrate) {
		var c *individual = new(individual)
		var d *individual = new(individual)
		c.genotype = make([]byte,len(a.genotype))
		d.genotype = make([]byte,len(a.genotype))
		for i:=0; i<len(a.genotype); i++ {
			maska := byte(rg.Intn(255))
			maskb := 255 ^ maska
			c.genotype[i] = (a.genotype[i] & maska) |
				(b.genotype[i] & maskb)
			d.genotype[i] = (a.genotype[i] & maskb) |
				(b.genotype[i] & maska)
		}
		return c,d
	} else {
		return a,b
	}
}


func recombineTwopoint (a,b *individual) (*individual,*individual) {
	if (rg.Float64()<*recrate) {
		var c *individual = new(individual)
		var d *individual = new(individual)
		c.genotype = make([]byte,len(a.genotype))
		d.genotype = make([]byte,len(a.genotype))
		links := rg.Intn(8*len(a.genotype)-1)
		rechts := rg.Intn(8*len(a.genotype)-1)
		if links>rechts {
			links,rechts = rechts,links
		}
		bytenuml := links/8
		posl := uint(links%8)
		bytenumr := rechts/8
		posr := uint(rechts%8)
		for i:=0; i<bytenuml; i++ {
			c.genotype[i] = a.genotype[i]
			d.genotype[i] = b.genotype[i]
		}
		for i:=bytenumr+1; i<len(a.genotype); i++ {
			c.genotype[i] = a.genotype[i]
			d.genotype[i] = b.genotype[i]
		}
		for i:=bytenuml+1; i<bytenumr; i++ {
			c.genotype[i] = b.genotype[i]
			d.genotype[i] = a.genotype[i]
		}
		if bytenuml==bytenumr {
			c.genotype[bytenuml],d.genotype[bytenuml] =
				twopointInOneByte(a.genotype[bytenuml],b.genotype[bytenuml],posl,posr)
		} else {
			c.genotype[bytenuml],d.genotype[bytenuml] =
				onepointInOneByte(a.genotype[bytenuml],b.genotype[bytenuml],posl)
			c.genotype[bytenumr],d.genotype[bytenumr] =
				onepointInOneByte(b.genotype[bytenumr],a.genotype[bytenumr],posr)
		}
		return c,d
	} else {
		return a,b
	}
}

func twopointInOneByte(a,b byte, i,j uint) (byte,byte) {
	maska := maskOnepointA[i] | maskOnepointB[j]
	maskb := maskOnepointB[i] & maskOnepointA[j]
	c := a&maska | b&maskb
	d := b&maska | a&maskb
	return c,d
}

func (p *population) computeSelectProbabilities() {
	minusterm := 0.0
	scaleterm := 1.0
	if (*selectionname == "fitpropscale") {
		bestval := p.indi[0].quality
		worstval := p.indi[0].quality
		for i:=1; i<*popsize; i++ {
			if p.indi[i].quality > bestval {
				bestval = p.indi[i].quality
			}
			if p.indi[i].quality < worstval {
				worstval = p.indi[i].quality
			}
		}
		minusterm = worstval
		scaleterm = bestval - worstval
	}
	total := float64(0.0)
	for i:=0; i<*popsize; i++ {
		total += (p.indi[i].quality - minusterm)/scaleterm
	}
	p.prob[0] = float64(((p.indi[0].quality  - minusterm)/scaleterm)/ total)
	for i:=1; i<*popsize; i++ {
		p.prob[i] = p.prob[i-1] + float64(((p.indi[i].quality - 
			minusterm)/scaleterm) / total)
	}
}

func selectFitprop (pop *population) *individual {
	rndptr := rg.Float64()
	index := 0
	for rndptr > pop.prob[index] {
		index++
	}
	if index>= *popsize { // if sum results in 0.99... and sh*t happens
		index = *popsize-1
	}
	return pop.indi[index]
}

func selectTournament (pop *population) *individual {
	var winner *individual = pop.indi[rg.Intn(*popsize)]
	for i:=1; i<*tournamentsize; i++ {
		challenger := pop.indi[rg.Intn(*popsize)]
		if challenger.quality > winner.quality {
			winner = challenger
		}
	}
	return winner
}

