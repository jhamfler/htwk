package main

import (
	"fmt"
	"time"
	"flag"
	"math"
	"math/rand"
	"qfunc"
)

var tmax = flag.Int("tmax", 2000, "maximal number of generations")
var dim = flag.Int("dim", 23, "dimension (tsp: 23,47; atsp: 47)")
var qfname = flag.String("problem","tsp","name of the quality function to be optimized - tsp, atsp, cfunc")
var variant = flag.String("variant","hc","kind of local search - hc, sa, ta, gd, rr")
var inittemp = flag.Float64("inittemp",100.0,"initial temperature for sa,ta,gd")
var alpha = flag.Float64("alpha",0.99,"modifier for temperature")
var mutationname = flag.String("mutation","switch","mutation operator - switch,invers,shift")
var compact = flag.Bool("compact",false,"compact output, improvement only")


type individual struct {
	genotype []int
	quality float64 
}


type mutationOp func(ind *individual) *individual

type acceptCriterion func(new, old float64) bool

var rg = rand.New(rand.NewSource(99))

var qf qfunc.QFunctionPerm

var temp float64
var bestfound float64

func NewIndividual() *individual {
	var ind individual
	var geno = make([]int,*dim)
	ind.genotype = geno
	for i:=0; i<*dim; i++ {
		geno[i] = i+1
	}
	for i:=1; i<*dim; i++ {
		k := rg.Intn(i+1)
		geno[i], geno[k] = geno[k], geno[i]
	}
	ind.quality = 0.0
	return &ind
}

func main() {
	flag.Parse()
	rg.Seed(time.Now().UnixNano())
	switch *qfname {
	case "tsp":
		qf = new(qfunc.Tsp).Init(*dim,false)
	case "cfunc":
		qf = new(qfunc.Cfunc).Init(*dim)
	default:
		panic("unrecognized quality function")
	}
	var mutate mutationOp 
	switch *mutationname {
	case "switch":
		mutate = mutateSwitch
	case "invers":
		mutate = mutateInvers
	case "shift":
		mutate = mutateShift
	default:
		panic("unrecognized mutation operator")
	}
	var accept acceptCriterion
	switch *variant {
	case "hc":
		accept = acceptHc
	case "sa":
		accept = acceptSa
		temp = *inittemp
	case "ta":
		accept = acceptTa
		temp = *inittemp
	case "gd":
		accept = acceptGd
		temp = *inittemp
	case "rr":
		accept = acceptRr
		temp = *inittemp
	default:
		panic("unrecognized mutation operator")
	}
	best, evaluations := localsearch(mutate, accept)
	printInd(best)
	fmt.Print("number of evaluations: ")
	fmt.Println(evaluations)
	qf.PrintPhenotype(best.genotype)
}


func printInd(ind *individual) {
	fmt.Print(ind.genotype)
	fmt.Print(" - ")
	if (*variant == "hc") {
		fmt.Println(ind.quality)
	} else {
		fmt.Print(ind.quality)
		fmt.Print(" # ")
		fmt.Println(temp)
	}
}

func localsearch(mutate mutationOp, accept acceptCriterion) (*individual, int) {
	t := 0
	ind := NewIndividual()
	ind.quality = qf.Evaluate(ind.genotype)
	if (*variant == "rr") {
		bestfound = ind.quality
	}
	printInd(ind)
	for !qf.IsOptimumReached(ind.quality) &&  t < *tmax {
		offspring := mutate(ind)
		offspring.quality = qf.Evaluate(offspring.genotype)
		isImprovement := false
		if accept(offspring.quality, ind.quality) {
			ind = offspring
			isImprovement = true
		}
		t++
		if (*compact) {
			if (isImprovement) {
				fmt.Print(t)
				fmt.Print(" ")
				printInd(ind)
			}
		} else {
			printInd(ind)
		}
	}
	return ind, t
}

func mutateSwitch (ind *individual) *individual {
	var offspring *individual = new(individual)
	var geno = make([]int,len(ind.genotype))
	offspring.genotype = geno
	for i:=0; i<len(geno); i++ {
		geno[i] = ind.genotype[i]
	}
	j := rg.Intn(len(geno))
	k := rg.Intn(len(geno))
	geno[j],geno[k] = geno[k],geno[j]
	return offspring
}

func mutateShift (ind *individual) *individual {
	var offspring *individual = new(individual)
	var geno = make([]int,len(ind.genotype))
	offspring.genotype = geno
	for i:=0; i<len(geno); i++ {
		geno[i] = ind.genotype[i]
	}
	j := rg.Intn(len(geno))
	k := rg.Intn(len(geno))
	shiftval := geno[j]
	if (j<k) {
		for j<k {
			geno[j] = geno[j+1]
			j++
		}
	} else {
		for j>k {
			geno[j] = geno[j-1]
			j--
		}
	}
	geno[k] = shiftval
	return offspring
}

func mutateInvers (ind *individual) *individual {
	var offspring *individual = new(individual)
	var geno = make([]int,len(ind.genotype))
	offspring.genotype = geno
	for i:=0; i<len(geno); i++ {
		geno[i] = ind.genotype[i]
	}
	j := rg.Intn(len(geno))
	k := rg.Intn(len(geno))
	if k<j {
		j,k = k,j
	}
	for j<k {
		geno[j],geno[k] = geno[k],geno[j]
		j++
		k--
	}
	return offspring
}

func acceptHc (new, old float64) bool {
	return qf.IsBetter(new, old)
}

func acceptSa (new, old float64) bool {
	temp *= *alpha
	if qf.IsBetter(new, old) {
		return true
	} else {
		u := rg.Float64()
		return u<=math.Exp(- math.Abs(new-old)/temp)
	}
}

func acceptTa (new, old float64) bool {
	temp *= *alpha
	return qf.IsBetter(new, old) || math.Abs(new-old)<=temp
}

func acceptGd (new, old float64) bool {
	temp -= *alpha
	return qf.IsBetter(new, temp)
}

func acceptRr (new, old float64) bool {
	temp *= *alpha
	if qf.IsBetter(new, bestfound) {
		bestfound = new
		return true
	} else {
		return math.Abs(new-bestfound)<temp
	}
}
