package qfunc

import ( 
	"fmt"
	"math"
)

type Cfunc struct  {
	dim int
	bestval float64
}

func (p *Cfunc) Init (dim int) *Cfunc {
	p.dim = dim
	p.bestval = math.MaxFloat64
	return p
}

func (p *Cfunc) IsBetter (new, old float64) bool {
	return (new > old)
}

func (p *Cfunc) IsOptimumReached (val float64) bool {
	return !p.IsBetter(p.bestval,val)
}

func (p *Cfunc) Evaluate (permutation []int) float64 {
	var quality float64 = 0.0
	for i:=0;i<len(permutation)-1;i++ {
		for k:=i+1;k<len(permutation);k++ {
			quality += ( math.Abs(float64(permutation[k]) -
				float64(permutation[i]))) /
				float64(k-i)
		}
	}
	return 2.0 * quality
}

func (p *Cfunc) PrintPhenotype (permutation []int) {
	fmt.Println(permutation)
}
