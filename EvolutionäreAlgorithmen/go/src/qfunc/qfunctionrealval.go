package qfunc

type QFunctionRealVal interface {
	Evaluate (objectvalues []float64) float64
	IsBetter (new, old float64) bool
	IsOptimumReached (val float64) bool
	PrintPhenotype (objectvalues []float64)
}
