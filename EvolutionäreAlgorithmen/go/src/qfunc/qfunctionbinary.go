package qfunc

type QFunctionBinary interface {
	Evaluate (genotype []byte) float64
	IsBetter (new, old float64) bool
	IsOptimumReached (val float64) bool
	PrintPhenotype (genotype []byte)
}
