package qfunc

import ( 
	"fmt"
	"math"
)

type Rastrigin struct  {
	dim int
	bestval float64
}

func (p *Rastrigin) Init (dim int) *Rastrigin {
	p.dim = dim
	p.bestval = 0.0
	return p
}

func (p *Rastrigin) IsBetter (new, old float64) bool {
	return (new < old)
}

func (p *Rastrigin) IsOptimumReached (val float64) bool {
	return !p.IsBetter(p.bestval,val)
}

func (p *Rastrigin) Evaluate (objectvalues []float64) float64 {
	var quality float64 = float64(10 * p.dim)
	for i:=0;i<len(objectvalues);i++ {
		quality += objectvalues[i]*objectvalues[i] - 10.0 * math.Cos(2.0*math.Pi * objectvalues[i])
	}
	return quality
}

func (p *Rastrigin) PrintPhenotype (objectvalues []float64) {
	fmt.Println(objectvalues)
}
