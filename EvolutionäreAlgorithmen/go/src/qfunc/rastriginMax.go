package qfunc

import ( 
	"fmt"
	"math"
)

type RastriginMax struct  {
	dim int
	bestval float64
}

func (p *RastriginMax) Init (dim int) *RastriginMax {
	p.dim = dim
	p.bestval = 25.12 * float64(dim)
	return p
}

func (p *RastriginMax) IsBetter (new, old float64) bool {
	return (new > old)
}

func (p *RastriginMax) IsOptimumReached (val float64) bool {
	return !p.IsBetter(p.bestval,val)
}

func (p *RastriginMax) Evaluate (objectvalues []float64) float64 {
	var quality float64 = float64(10 * p.dim)
	for i:=0;i<len(objectvalues);i++ {
		quality += objectvalues[i]*objectvalues[i] - 10.0 * math.Cos(2.0*math.Pi * objectvalues[i])
	}
	return p.bestval - quality
}

func (p *RastriginMax) PrintPhenotype (objectvalues []float64) {
	fmt.Println(objectvalues)
}
