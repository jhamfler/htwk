package qfunc
 
import ( 
	"fmt"
	"bytes"
	"strconv"
)

type item struct {
    w, v int
}
 
// Kreher and Stinson, with an optimal profit of 13549094
// Loesung: 110111000110100100000111
var items1 []item = []item{
	{382745,  825594},
	{799601, 1677009},
	{909247, 1676628},
	{729069, 1523970},
	{467902,  943972},
	{ 44328,   97426},
	{ 34610,   69666},
	{698150, 1296457},
	{823460, 1679693},
	{903959, 1902996},
	{853665, 1844992},
	{551830, 1049289},
	{610856, 1252836},
	{670702, 1319836},
	{488960,  953277},
	{951111, 2067538},
	{323046,  675367},
	{446298,  853655},
	{931161, 1826027},
	{ 31385,   65731},
	{496951,  901489},
	{264724,  577243},
	{224916,  466257},
	{w:169684,  v:369261},
}
 
type Knapsack struct  {
	dim int
	bestval float64
	capacity int
	items []item
	onlylegal bool
}

/*knapPI_1_50_1000_1			
n 50			
c 995			
z 8373			
time 0.00*/

var items2 []item = []item{	
	{v:94, w:485}, //	0
	{v:506, w:326}, //	0
	{v:416, w:248}, //	0
	{v:992, w:421}, //	0
	{v:649, w:322}, //	0
	{v:237, w:795}, //	0
	{v:457, w:43}, //	1
	{v:815, w:845}, //	0
	{v:446, w:955}, //	0
	{v:422, w:252}, //	0
	{v:791, w:9}, //	1
	{v:359, w:901}, //	0
	{v:667, w:122}, //	1
	{v:598, w:94}, //	1
	{v:7, w:738}, //	0
	{v:544, w:574}, //	0
	{v:334, w:715}, //	0
	{v:766, w:882}, //	0
	{v:994, w:367}, //	0
	{v:893, w:984}, //	0
	{v:633, w:299}, //	0
	{v:131, w:433}, //	0
	{v:428, w:682}, //	0
	{v:700, w:72}, //	1
	{v:617, w:874}, //	0
	{v:874, w:138}, //	1
	{v:720, w:856}, //	0
	{v:419, w:145}, //	0
	{v:794, w:995}, //	0
	{v:196, w:529}, //	0
	{v:997, w:199}, //	1
	{v:116, w:277}, //	0
	{v:908, w:97}, //	1
	{v:539, w:719}, //	0
	{v:707, w:242}, //	0
	{v:569, w:107}, //	0
	{v:537, w:122}, //	0
	{v:931, w:70}, //	1
	{v:726, w:98}, //	1
	{v:487, w:600}, //	0
	{v:772, w:645}, //	0
	{v:513, w:267}, //	0
	{v:81, w:972}, //	0
	{v:943, w:895}, //	0
	{v:58, w:213}, //	0
	{v:303, w:748}, //	0
	{v:764, w:487}, //	0
	{v:536, w:923}, //	0
	{v:724, w:29}, //	1
	{v:789, w:674}, //	0
        {v:1, w:1000},
	{v:2, w:1000},
	{v:3, w:1000},
	{v:4, w:1000},
	{v:5, w:1000},
	{v:6, w:1000},
}


var mask [8]byte = [8]byte{1,2,4,8,16,32,64,128}
 
func (p *Knapsack) Init (dim int, onlylegal bool) *Knapsack {
	if dim <= 3 {
		p.dim = 3
		p.bestval = 13549094.0
		p.items = items1
		p.capacity = 6404180
	} else {
		p.dim = 7
		p.bestval = 8373.0
		p.items = items2
		p.capacity = 995
	}
	p.onlylegal = onlylegal
	return p
}

func (p *Knapsack) GetDim () int {
	return p.dim
}

func (p *Knapsack) IsBetter (new, old float64) bool {
	return (new >= old)
}

func (p *Knapsack) IsOptimumReached (val float64) bool {
	return (p.bestval <= val)
}

func (p *Knapsack) Evaluate (genotype []byte) float64 {
	weight := 0
	value := 0
	iter := -1
	for _,g := range genotype {
		iter = iter+1
		for i:= 0; i<8; i++ {
			if (g&mask[(7-i)]>0) {
				newweight := weight+p.items[iter*8+i].w
				if (!p.onlylegal || newweight<=p.capacity ) {
					weight = newweight
					value = value+p.items[iter*8+i].v
				}
			}
		}
	}
	//fmt.Println(weight)
	if weight <= p.capacity {
		return float64(value)
	}
        //fmt.Print(" .. ")
        //fmt.Println(float64(p.capacity-weight))
	return float64(p.capacity-weight)
}


func (p *Knapsack) PrintPhenotype (genotype []byte) {
	var itembuffer bytes.Buffer
	var valuebuffer bytes.Buffer
	var weightbuffer bytes.Buffer
	itembuffer.WriteString("included items: ")
	valuebuffer.WriteString("sum of values: ")
	weightbuffer.WriteString("sum of weights: ")
	weight := 0
	value := 0
	iter := -1
	for _,g := range genotype {
		iter = iter+1
		for i:= 0; i<8; i++ {
			if (g&mask[(7-i)]>0) {
				newweight := weight+p.items[iter*8+i].w
				if (!p.onlylegal || newweight<=p.capacity ) {
					itembuffer.WriteString(strconv.FormatInt(int64(iter*8+i),10))
					itembuffer.WriteString(" ")
					weight = newweight
					weightbuffer.WriteString(strconv.FormatInt(int64(p.items[iter*8+i].w),10))
					weightbuffer.WriteString(" ")
					value = value+p.items[iter*8+i].v
					valuebuffer.WriteString(strconv.FormatInt(int64(p.items[iter*8+i].v),10))
					valuebuffer.WriteString(" ")
				}
			}
		}
	}
	valuebuffer.WriteString("= ");
	valuebuffer.WriteString(strconv.FormatInt(int64(value),10))
	weightbuffer.WriteString("= ");
	weightbuffer.WriteString(strconv.FormatInt(int64(weight),10))
	fmt.Println(itembuffer.String())
	fmt.Println(valuebuffer.String())
	fmt.Println(weightbuffer.String())
	fmt.Print("capacity: ")
	fmt.Println(p.capacity);
	if weight > p.capacity {
		fmt.Println("exceeds capacity!")
	}
}
