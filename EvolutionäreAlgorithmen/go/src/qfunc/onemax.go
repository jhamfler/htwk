package qfunc

//import (
//	"direction"
//)

type Onemax struct  {
	dim int
	bestval float64
}

var onesInByte map[byte]int 

func (p *Onemax) Init (dim int) *Onemax {
	p.dim = dim
	p.bestval = float64(8*p.dim)
	var mask [8]byte = [8]byte{1,2,4,8,16,32,64,128}
	onesInByte = make(map[byte]int)
	var k int
	for k=0; k<=255; k++ {
		count := 0
		theByte := byte(k)
		for i:= 0; i<8; i++ {
			if theByte&mask[i]>0 {
				count++
			}
		}
		onesInByte[theByte] = count
	}
	return p
//	return direction.Bigger,float64(8*dim)
}

func (p *Onemax) IsBetter (new, old float64) bool {
	return (new > old)
}

func (p *Onemax) IsOptimumReached (val float64) bool {
	return !p.IsBetter(p.bestval,val)
}

func (p *Onemax) Evaluate (genotype []byte) float64 {
     ones := 0
     for _, g := range genotype {
     	 ones += onesInByte[g]
     }
//     fmt.Println(ones)
	return float64(ones)
//     ind.quality = float64(ones)
}


func (p *Onemax) PrintPhenotype (genotype []byte) {
	
}
