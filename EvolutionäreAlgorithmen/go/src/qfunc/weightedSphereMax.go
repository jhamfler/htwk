package qfunc

import ( "fmt" )

type WeightedSphereMax struct  {
	dim int
	bestval float64
}

func (p *WeightedSphereMax) Init (dim int) *WeightedSphereMax {
	p.dim = dim
	p.bestval =  float64((p.dim * (p.dim+1) * (2*p.dim+1))/6) * 5.12 * 5.12
	return p
}

func (p *WeightedSphereMax) IsBetter (new, old float64) bool {
	return (new > old)
}

func (p *WeightedSphereMax) IsOptimumReached (val float64) bool {
	return !p.IsBetter(p.bestval,val)
}

func (p *WeightedSphereMax) Evaluate (objectvalues []float64) float64 {
	var quality float64 = 0.0
	for i:=0;i<len(objectvalues);i++ {
		quality += float64((i+1)*(i+1))*objectvalues[i]*objectvalues[i]
	}
	return p.bestval - quality
}

func (p *WeightedSphereMax) PrintPhenotype (objectvalues []float64) {
	fmt.Println(objectvalues)
}
