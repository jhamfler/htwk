package qfunc

//import (
//	"direction"
//)

type Hiff struct  {
	dim int
	bestval float64
}

var maskhiff []byte = []byte{192,48,12,3,240,15}

type allvals byte

const (
	ones allvals = iota
	zeros
	divers
)

var hycnt []allvals

func (p *Hiff) Init (dim int) *Hiff {
	p.dim = dim
	p.bestval = float64(8*p.dim-1)
	hycnt = make([]allvals,dim)
	return p
}

func (p *Hiff) IsBetter (new, old float64) bool {
	return (new > old)
}

func (p *Hiff) IsOptimumReached (val float64) bool {
	return !p.IsBetter(p.bestval,val)
}

func (p *Hiff) Evaluate (genotype []byte) float64 {
	count := 0
	index := 0
	for _, g := range genotype {
		for i:=0; i<6; i++ {
			if (g&maskhiff[0] == maskhiff[0]) || (g&maskhiff[0] == 0) {
				count++
			}
		}
		if g==255 {
			hycnt[index] = ones
			count++
		} else {
			if g==0 {
				hycnt[index] = zeros
				count++
			} else {
				hycnt[index] = divers
			}
		}
		index++
	}
	for i:=p.dim;i>1;i=i/2 {
		for j:=0; j<i; j=j+2 {
			if (hycnt[j]==ones && hycnt[j+1]==ones) {
				count++
				hycnt[j/2]=ones
			} else {
				if(hycnt[j]==zeros && hycnt[j+1]==zeros) {
					count++
					hycnt[j/2]=zeros
				} else {
					hycnt[j/2]=divers
				}
			}
		}
	}
//     fmt.Println(ones)
	return float64(count)
//     ind.quality = float64(ones)
}


func (p *Hiff) PrintPhenotype (genotype []byte) {
	
}
