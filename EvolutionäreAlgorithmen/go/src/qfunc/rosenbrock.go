package qfunc

import ( 
	"fmt"
)

type Rosenbrock struct  {
	dim int
	bestval float64
}

func (p *Rosenbrock) Init (dim int) *Rosenbrock {
	p.dim = dim
	p.bestval = 0.0
	return p
}

func (p *Rosenbrock) IsBetter (new, old float64) bool {
	return (new < old)
}

func (p *Rosenbrock) IsOptimumReached (val float64) bool {
	return !p.IsBetter(p.bestval,val)
}

func (p *Rosenbrock) Evaluate (objectvalues []float64) float64 {
	var quality float64 = float64(0.0)
	for i:=0;i<len(objectvalues)-1;i++ {
		quality += 100*(objectvalues[i]*objectvalues[i]-objectvalues[i+1])*(objectvalues[i]*objectvalues[i]-objectvalues[i+1]) + (1.0 - objectvalues[i])*(1.0 - objectvalues[i])
	}
	return quality
}

func (p *Rosenbrock) PrintPhenotype (objectvalues []float64) {
	fmt.Println(objectvalues)
}
