package qfunc

type QFunctionPerm interface {
	Evaluate (permutation []int) float64
	IsBetter (new, old float64) bool
	IsOptimumReached (val float64) bool
	PrintPhenotype (permutation []int)
}
