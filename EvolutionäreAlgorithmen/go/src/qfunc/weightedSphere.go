package qfunc

import ( "fmt" )

type WeightedSphere struct  {
	dim int
	bestval float64
}

func (p *WeightedSphere) Init (dim int) *WeightedSphere {
	p.dim = dim
	p.bestval = 0.0
	return p
}

func (p *WeightedSphere) IsBetter (new, old float64) bool {
	return (new < old)
}

func (p *WeightedSphere) IsOptimumReached (val float64) bool {
	return !p.IsBetter(p.bestval,val)
}

func (p *WeightedSphere) Evaluate (objectvalues []float64) float64 {
	var quality float64 = 0.0
	for i:=0;i<len(objectvalues);i++ {
		quality += float64((i+1)*(i+1))*objectvalues[i]*objectvalues[i]
	}
	return quality
}

func (p *WeightedSphere) PrintPhenotype (objectvalues []float64) {
	fmt.Println(objectvalues)
}
