#include <stdio.h>
#include <string.h> // strlen
#include "../hilfswerkzeuge/zeitmessung.c"

void search(char pat[], char txt[]);

int main(int argc, char * argv[]) {
    // Parameter testen
	  if(argc != 3) {
		  printf ("Benutzung: ./primitiv Quelldatei Suchstring");
		  return 1;
	  }

	long long t=0;
	t=zeitmessung(&search, argv[1], argv[2]);
	printf("%lld\n",t);

	return EXIT_SUCCESS;
}

void search(char pat[], char txt[])
{
    int M = strlen(pat);
    int N = strlen(txt);
    int i, j, g;

    // Substring um eins verschieben
    for (i = 0; i <= N - M; i++) {
      g=1;
      for (j = 0; j < M; j++) {
	if (txt[i+j] != pat[j]) {
          g=0;
          //if (j == M) printf("Match an Stelle %d\n", i);
        }
      }
      if (g) {} // match gefunden
    }
}
