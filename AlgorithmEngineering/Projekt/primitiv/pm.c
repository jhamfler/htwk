#include <stdio.h>
#include <string.h> // strlen
#include "../hilfswerkzeuge/zeitmessungm.c"

void search(char * pat[], char txt[], int anzahlsuchbegriffe);
unsigned long long ReadFileSize(char *filename);

int main(int argc, char * argv[]) {
    // Parameter testen
	  if(argc < 3) {
			//					1			2			3		4
		  printf ("Benutzung: ./primitiv Quelldatei Suchstring1 Suchstring2");
		  return 1;
	  }

	char *suchstrings[argc-2];
	for (int i=0; i<argc-2; i++) {
		suchstrings[i] = argv[i+2];
	}

	long long t=0;
	t=zeitmessung(&search, argv[1], suchstrings, argc-2);
	printf("%lld\n",t);

	return EXIT_SUCCESS;
}

void search(char * pat[], char txt[], int anzahlsuchbegriffe)
{
	int M;
    int N = strlen(txt);
    int i, j, k;

    for (k=0; k<anzahlsuchbegriffe; k++) {
      M = strlen(pat[k]);
      // Substring um eins verschieben
      for (i=0; i<=N-M; i++) {
        for (j=0; j<M; j++) {
          if (txt[i+j] != pat[k][j]) {
            break;
            //if (j == M) printf("Match an Stelle %d\n", i);
          }
        }
	  }
    }
}

unsigned long long ReadFileSize(char *filename) {
   unsigned long long string_size;
   FILE *handler = fopen(filename, "r");

   if (handler) {
       // Seek the last byte of the file
       fseek(handler, 0, SEEK_END);

       // Offset from the first to the last byte, or in other words, filesize
       string_size = ftell(handler);
       fclose(handler);
    }

    return string_size;
}

