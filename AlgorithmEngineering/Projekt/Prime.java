class Prime {
public static void main (String args[]) {

	long n=498560650640798699L;

if (n < 2) {
    System.out.println("Number must be greater than 1");
} else {
    while (!prim(n)) n-=2;
    System.out.println(n);
}
}

public static boolean prim(long n){
        for(long i=2;i<n/2;i++){
            if(n%i == 0){
                return false;
            }
        }
        return true;
    }
}
