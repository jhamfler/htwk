#!/bin/bash
directory=txt/gen3
anzahlmessreihen=25
mkdir -p $directory
byte=1000
kilo=100
mega=1
#100kb bis 2,5mb
for i in $(seq 1 $anzahlmessreihen)
do
	base64 /dev/urandom | grep '[[:alpha:]]*' -o | grep '[[:lower:]]' -o | tr -d '\n' | head -c $((i*$mega*$kilo*$byte)) > "$directory/$i.txt"
done
