#include <stdio.h>
#include <string.h> // strlen
#include "../hilfswerkzeuge/zeitmessungm.c" // Zeit messen

#define DEBUG 0

void search(char *pat[], char txt[], int anzahlsuchbegriffe);


int main(int argc, char * argv[])
{
	if(argc < 3) {
		//					1	 2			3
		printf ("Benutzung: ./rk Quelldatei Suchstring1 Suchstring2 ...");
		return EXIT_FAILURE;
	}

	char *suchstrings[argc-2];
	for (int i=0; i<argc-2; i++) {
		suchstrings[i] = argv[i+2];
	}

	long long t=0;
	t=zeitmessung(&search, argv[1], suchstrings, argc-2);
	printf("%lld\n",t);

	return EXIT_SUCCESS;
}

void search(char ** pat, char text[], int anzahlsuchbegriffe) {
	unsigned char * txt = (unsigned char *) text;

	unsigned int d = 26;	// Anzahl der Elemente des Alphabets
//    unsigned long long q = 104723; // Primzahl für Modulo
	unsigned long long q = 18446744073709551557; // größte Primzahl <2^64
	unsigned long long N = strlen(txt);
    unsigned long long i, j, k, len; // len == Länge der unterschiedlichen Suchstrings

	unsigned long long M[anzahlsuchbegriffe]; // Länge der Suchbegriffe
    unsigned long long p[anzahlsuchbegriffe]; // Hashwerte der Suchbegriffe

	// init
	for (i=0; i<anzahlsuchbegriffe; i++) {
		M[i]=strlen(pat[i]); // Länge des Suchstrings
		p[i] = 0; // Hash des Suchstrings
	}

	// Sortiere Suchbegriffe nach Länge
	for (i=0; i<anzahlsuchbegriffe; i++) {
		for (j=0; j<anzahlsuchbegriffe-i-1; j++) {
			if (M[j] > M[j+1]) {
				// tausche Index
				k = M[j];
				M[j] = M[j+1];
				M[j+1] = k;
				// tausche Zeiger
				char * c = pat[j];
				pat[j] = pat[j+1];
				pat[j+1] = c;
			}
		}
	}

	// Prüfe ob zu lange Pattern vorhanden sind
	k=anzahlsuchbegriffe;
	for (i=0; i<k; i++) {
	    if (N<M[i]) anzahlsuchbegriffe--;
	}

	// bestimme Anzahl unterschiedlicher Längen
	if (anzahlsuchbegriffe<1) return;
	len=1;
	if (anzahlsuchbegriffe!=1) {
		for (i=0; i<anzahlsuchbegriffe-1; i++) {
			if (M[i]!=M[i+1]) len++;
		}
	}

	// init Feld für unterschiedliche Längen
	unsigned long long lens[len];
	lens[0]=0;
	for (i=0, j=0; i<anzahlsuchbegriffe-1; i++) {
		if (M[i]!=M[i+1]) lens[++j]=i+1;
		else lens[j]=i+1;
	}

	// init Feld für unterschiedliche Längen in umgekehrtem Sinn
	unsigned long long ulens[anzahlsuchbegriffe];
	ulens[0]=0;
	for (i=0, j=0; i<anzahlsuchbegriffe-1; i++) {
		if (M[i]!=M[i+1]) ulens[i+1]=++j;
		else ulens[i+1]=j;
	}

	// Stellenwert aller Suchstringlängen
	unsigned long long h[len];
	for (k=0; k<len; k++) {
		h[k]=1;
	}
	for (k=0; k<len; k++) {
		for (j=1; j<M[lens[k]]; j++) {
			h[k]=(h[k]*d)%q;
		}
	}

    // Hash aller Suchstrings
	for (k=0; k<anzahlsuchbegriffe; k++) {
	    for (i=0; i<M[k]; i++) {
    	    p[k] = (d*p[k] + pat[k][i])%q;
		}
    }

	// init Substring Hashwerte für jede Länge
	unsigned long long t[len];
	for (i=0; i<len; i++) {
		t[i]=0;
	}

	// Hash der ersten Substrings
	for (k=0; k<len; k++) {
    	for (i=0; i<M[lens[k]]; i++) {
        	t[k] = (d*t[k] + txt[i])%q;
		}
	}

    // Substring um eins verschieben
    for (i=0; i <= N-M[lens[len-1]]; i++) { // N - längster Suchstring
        // Substringhashes prüfen
		for (k=0; k<anzahlsuchbegriffe; k++) {
        	if ( p[k] == t[ulens[k]] ) {
            	// Zeichen prüfen
            	for (j=0; j<M[k]; j++) {
                	if (txt[i+j] != pat[k][j]) {
                    	break;
					}
					// wenn p == t && Suchstring == Substring && Ende des Vergleichs erreicht
					if (DEBUG) if (j == M[k]-1) printf("Match an Stelle %d\n", i);
				}
        	}
		}

        // nächste Hashwerte der Substrings berechnen
		for (k=0; k<len; k++) {
			//printf("k: %lld\n",k);
			if ( i < N-M[lens[k]] ) {
				//printf("i: %lld\t",i);
				t[k] = (d*(t[k] - txt[i]*h[k]) + txt[i+M[lens[k]]])%q;
				// printf("%d", t[k]);
			}
		}
	}

	// zweite Runde, wenn der längste Suchstring zu lang ist
	// macht den Code schneller für große Suchtexte
    // Substring um eins verschieben
    for (i=N-M[lens[len-1]]+1; i <= N-M[lens[0]]; i++) { // ab letzte Position bis N - kürzester Suchstring
		// prüfen ob der längste Substring am Ende des Textes angekommen ist
		if (i<=N-M[lens[len-1]]) {
			if (len>0) {
				len--;
				// entferne die letzten Suchstrings mit ger größten Länge
				anzahlsuchbegriffe=lens[len];
			} else {
				return;
			}
		}

        // Substringhashes prüfen
		for (k=0; k<anzahlsuchbegriffe; k++) {
        	if ( p[k] == t[ulens[k]] ) {
            	// Zeichen prüfen
            	for (j=0; j<M[k]; j++) {
                	if (txt[i+j] != pat[k][j]) {
                    	break;
					}
					// wenn p == t && Suchstring == Substring && Ende des Vergleichs erreicht
					if (DEBUG) if (j == M[k]-1) printf("Match an Stelle %d\n", i);
				}
        	}
		}

        // nächste Hashwerte der Substrings berechnen
		for (k=0; k<len; k++) {
			if ( i < N-M[lens[k]] ) {
				t[k] = (d*(t[k] - txt[i]*h[k]) + txt[i+M[lens[k]]])%q;
				// printf("%d", t[k]);
			}
		}
	}
}
