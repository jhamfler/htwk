#include <stdio.h>
#include <string.h> // strlen
//#include "readfile.c" // Datei einlesen
#include "../hilfswerkzeuge/zeitmessung.c" // Zeit messen

#define d 26 // d = Anzahl der Elemente des Alphabets

void search(char pat[], char txt[]);


int main(int argc, char * argv[])
{
	if(argc != 3) {
		printf ("Benutzung: ./rk Quelldatei Suchstring");
		return EXIT_FAILURE;
	}

	long long t=0;
	t=zeitmessung(&search, argv[1], argv[2]);
    printf("%lld\n",t);

    return EXIT_SUCCESS;
}

void search(char pat[], char txt[]) {
    unsigned long long q = 104723;
    unsigned long long M = strlen(pat);
    unsigned long long N = strlen(txt);
    unsigned long long i, j, h = 1;
    long long p = 0; // Hash des Suchstrings
    long long t = 0; // Hash des Substrings

    // h = d^(M-1) % q
    for (i = 0; i < M-1; i++)
        h = (h*d)%q;

    // Hash des Suchstrings und ersten Substrings
    for (i = 0; i < M; i++) {
        p = (d*p + pat[i])%q;
        t = (d*t + txt[i])%q;
    }

    // Substring um eins verschieben
    for (i = 0; i <= N - M; i++) {
        // Substringhash prüfen
        // Wenn Match gefunden, prüfe Zeichen
        if ( p == t ) {
            // Zeichen prüfen
            for (j = 0; j < M; j++)
                if (txt[i+j] != pat[j])
                    break;

            // wenn p == t && Suchstring == Substring && Ende des Vergleichs erreicht
            //if (j == M) printf("Match an Stelle %d\n", i);
        }

        // Nächsten Hash des Substrings berechnen
        if ( i < N-M ) {
            t = (d*(t - txt[i]*h) + txt[i+M])%q;
//            printf("%d", t);
            // Wenn negativer Wert vorhanden ist
            if (t < 0)
              t = (t + q);
        }
    }
}
