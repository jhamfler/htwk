#include <stdio.h>
#include <string.h> // strlen
#include "../hilfswerkzeuge/zeitmessungm.c" // Zeit messen
#include "WuManber.cpp"

void search(char *pat[], char txt[], int anzahlsuchbegriffe);

int main(int argc, char * argv[])
{
	if(argc < 3) {
		//					1	 2			3
		printf ("Benutzung: ./rk Quelldatei Suchstring1 Suchstring2 ...");
		return EXIT_FAILURE;
	}

	char *suchstrings[argc-2];
	for (int i=0; i<argc-2; i++) {
			suchstrings[i] = argv[i+2];
	}

	long long t=0;
	t=zeitmessung(&search, argv[1], suchstrings, argc-2);
	printf("%lld\n",t);

	return EXIT_SUCCESS;
}

void search(char ** pat, char txt[], int anzahlsuchbegriffe) {
	vector<const char*> patterns;
	for (int i=0; i<anzahlsuchbegriffe; i++) {
		patterns.push_back(pat[i]);
	}
	WuManber wm;
	wm.Initialize(patterns);
	wm.Search(strlen(txt), txt, patterns);
}
