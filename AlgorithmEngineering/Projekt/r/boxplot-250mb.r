
rk = read.table("/home/jh/git/htwk/AlgorithmEngineering/Projekt/messwerte/rk.t.gen", header = TRUE)
pdf("/home/jh/git/htwk/AlgorithmEngineering/Projekt/graphen/rk.1.gen.pdf")
boxplot(rk$Time ~ rk$Targetsize, rk, main = "Methode: rk", sub="Suchbegriff: t", xlab = "Zielgröße", ylab = "benötigte Zeit")
dev.off()

rk = read.table("/home/jh/git/htwk/AlgorithmEngineering/Projekt/messwerte/rk.xr.gen", header = TRUE)
pdf("/home/jh/git/htwk/AlgorithmEngineering/Projekt/graphen/rk.2.gen.pdf")
boxplot(rk$Time ~ rk$Targetsize, rk, main = "Methode: rk", sub="Suchbegriff: xr", xlab = "Zielgröße", ylab = "benötigte Zeit")
dev.off()

rk = read.table("/home/jh/git/htwk/AlgorithmEngineering/Projekt/messwerte/rk.ytp.gen", header = TRUE)
pdf("/home/jh/git/htwk/AlgorithmEngineering/Projekt/graphen/rk.3.gen.pdf")
boxplot(rk$Time ~ rk$Targetsize, rk, main = "Methode: rk", sub="Suchbegriff: ytp", xlab = "Zielgröße", ylab = "benötigte Zeit")
dev.off()

rk = read.table("/home/jh/git/htwk/AlgorithmEngineering/Projekt/messwerte/rk.mzak.gen", header = TRUE)
pdf("/home/jh/git/htwk/AlgorithmEngineering/Projekt/graphen/rk.4.gen.pdf")
boxplot(rk$Time ~ rk$Targetsize, rk, main = "Methode: rk", sub="Suchbegriff: mzak", xlab = "Zielgröße", ylab = "benötigte Zeit")
dev.off()

rk = read.table("/home/jh/git/htwk/AlgorithmEngineering/Projekt/messwerte/rk.zmycy.gen", header = TRUE)
pdf("/home/jh/git/htwk/AlgorithmEngineering/Projekt/graphen/rk.5.gen.pdf")
boxplot(rk$Time ~ rk$Targetsize, rk, main = "Methode: rk", sub="Suchbegriff: zmycy", xlab = "Zielgröße", ylab = "benötigte Zeit")
dev.off()

rk = read.table("/home/jh/git/htwk/AlgorithmEngineering/Projekt/messwerte/rk.jeqyds.gen", header = TRUE)
pdf("/home/jh/git/htwk/AlgorithmEngineering/Projekt/graphen/rk.6.gen.pdf")
boxplot(rk$Time ~ rk$Targetsize, rk, main = "Methode: rk", sub="Suchbegriff: jeqyds", xlab = "Zielgröße", ylab = "benötigte Zeit")
dev.off()

rk = read.table("/home/jh/git/htwk/AlgorithmEngineering/Projekt/messwerte/rk.moeaicf.gen", header = TRUE)
pdf("/home/jh/git/htwk/AlgorithmEngineering/Projekt/graphen/rk.7.gen.pdf")
boxplot(rk$Time ~ rk$Targetsize, rk, main = "Methode: rk", sub="Suchbegriff: moeaicf", xlab = "Zielgröße", ylab = "benötigte Zeit")
dev.off()

rk = read.table("/home/jh/git/htwk/AlgorithmEngineering/Projekt/messwerte/rk.tbpixdoj.gen", header = TRUE)
pdf("/home/jh/git/htwk/AlgorithmEngineering/Projekt/graphen/rk.8.gen.pdf")
boxplot(rk$Time ~ rk$Targetsize, rk, main = "Methode: rk", sub="Suchbegriff: tbpixdoj", xlab = "Zielgröße", ylab = "benötigte Zeit")
dev.off()

rk = read.table("/home/jh/git/htwk/AlgorithmEngineering/Projekt/messwerte/rk.grfyqspic.gen", header = TRUE)
pdf("/home/jh/git/htwk/AlgorithmEngineering/Projekt/graphen/rk.9.gen.pdf")
boxplot(rk$Time ~ rk$Targetsize, rk, main = "Methode: rk", sub="Suchbegriff: grfyqspic", xlab = "Zielgröße", ylab = "benötigte Zeit")
dev.off()

rk = read.table("/home/jh/git/htwk/AlgorithmEngineering/Projekt/messwerte/rk.nchnsumkgu.gen", header = TRUE)
pdf("/home/jh/git/htwk/AlgorithmEngineering/Projekt/graphen/rk.10.gen.pdf")
boxplot(rk$Time ~ rk$Targetsize, rk, main = "Methode: rk", sub="Suchbegriff: nchnsumkgu", xlab = "Zielgröße", ylab = "benötigte Zeit")
dev.off()

primitiv = read.table("/home/jh/git/htwk/AlgorithmEngineering/Projekt/messwerte/primitiv.t.gen", header = TRUE)
pdf("/home/jh/git/htwk/AlgorithmEngineering/Projekt/graphen/primitiv.1.gen.pdf")
boxplot(primitiv$Time ~ primitiv$Targetsize, primitiv, main = "Methode: primitiv", sub="Suchbegriff: t", xlab = "Zielgröße", ylab = "benötigte Zeit")
dev.off()

primitiv = read.table("/home/jh/git/htwk/AlgorithmEngineering/Projekt/messwerte/primitiv.xr.gen", header = TRUE)
pdf("/home/jh/git/htwk/AlgorithmEngineering/Projekt/graphen/primitiv.2.gen.pdf")
boxplot(primitiv$Time ~ primitiv$Targetsize, primitiv, main = "Methode: primitiv", sub="Suchbegriff: xr", xlab = "Zielgröße", ylab = "benötigte Zeit")
dev.off()

primitiv = read.table("/home/jh/git/htwk/AlgorithmEngineering/Projekt/messwerte/primitiv.ytp.gen", header = TRUE)
pdf("/home/jh/git/htwk/AlgorithmEngineering/Projekt/graphen/primitiv.3.gen.pdf")
boxplot(primitiv$Time ~ primitiv$Targetsize, primitiv, main = "Methode: primitiv", sub="Suchbegriff: ytp", xlab = "Zielgröße", ylab = "benötigte Zeit")
dev.off()

primitiv = read.table("/home/jh/git/htwk/AlgorithmEngineering/Projekt/messwerte/primitiv.mzak.gen", header = TRUE)
pdf("/home/jh/git/htwk/AlgorithmEngineering/Projekt/graphen/primitiv.4.gen.pdf")
boxplot(primitiv$Time ~ primitiv$Targetsize, primitiv, main = "Methode: primitiv", sub="Suchbegriff: mzak", xlab = "Zielgröße", ylab = "benötigte Zeit")
dev.off()

primitiv = read.table("/home/jh/git/htwk/AlgorithmEngineering/Projekt/messwerte/primitiv.zmycy.gen", header = TRUE)
pdf("/home/jh/git/htwk/AlgorithmEngineering/Projekt/graphen/primitiv.5.gen.pdf")
boxplot(primitiv$Time ~ primitiv$Targetsize, primitiv, main = "Methode: primitiv", sub="Suchbegriff: zmycy", xlab = "Zielgröße", ylab = "benötigte Zeit")
dev.off()

primitiv = read.table("/home/jh/git/htwk/AlgorithmEngineering/Projekt/messwerte/primitiv.jeqyds.gen", header = TRUE)
pdf("/home/jh/git/htwk/AlgorithmEngineering/Projekt/graphen/primitiv.6.gen.pdf")
boxplot(primitiv$Time ~ primitiv$Targetsize, primitiv, main = "Methode: primitiv", sub="Suchbegriff: jeqyds", xlab = "Zielgröße", ylab = "benötigte Zeit")
dev.off()

primitiv = read.table("/home/jh/git/htwk/AlgorithmEngineering/Projekt/messwerte/primitiv.moeaicf.gen", header = TRUE)
pdf("/home/jh/git/htwk/AlgorithmEngineering/Projekt/graphen/primitiv.7.gen.pdf")
boxplot(primitiv$Time ~ primitiv$Targetsize, primitiv, main = "Methode: primitiv", sub="Suchbegriff: moeaicf", xlab = "Zielgröße", ylab = "benötigte Zeit")
dev.off()

primitiv = read.table("/home/jh/git/htwk/AlgorithmEngineering/Projekt/messwerte/primitiv.tbpixdoj.gen", header = TRUE)
pdf("/home/jh/git/htwk/AlgorithmEngineering/Projekt/graphen/primitiv.8.gen.pdf")
boxplot(primitiv$Time ~ primitiv$Targetsize, primitiv, main = "Methode: primitiv", sub="Suchbegriff: tbpixdoj", xlab = "Zielgröße", ylab = "benötigte Zeit")
dev.off()

primitiv = read.table("/home/jh/git/htwk/AlgorithmEngineering/Projekt/messwerte/primitiv.grfyqspic.gen", header = TRUE)
pdf("/home/jh/git/htwk/AlgorithmEngineering/Projekt/graphen/primitiv.9.gen.pdf")
boxplot(primitiv$Time ~ primitiv$Targetsize, primitiv, main = "Methode: primitiv", sub="Suchbegriff: grfyqspic", xlab = "Zielgröße", ylab = "benötigte Zeit")
dev.off()

primitiv = read.table("/home/jh/git/htwk/AlgorithmEngineering/Projekt/messwerte/primitiv.nchnsumkgu.gen", header = TRUE)
pdf("/home/jh/git/htwk/AlgorithmEngineering/Projekt/graphen/primitiv.10.gen.pdf")
boxplot(primitiv$Time ~ primitiv$Targetsize, primitiv, main = "Methode: primitiv", sub="Suchbegriff: nchnsumkgu", xlab = "Zielgröße", ylab = "benötigte Zeit")
dev.off()

