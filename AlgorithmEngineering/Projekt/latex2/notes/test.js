var Swagger = require('swagger-client');
var client = new Swagger({
  url: 'http://liveapi.dreamfactory.com/api/v2/api_docs',
  authorizations : {
    someHeaderAuth: new Swagger.ApiKeyAuthorization('X-DreamFactory-Api-Key', '36fda24fe5588fa4285ac6c6c2fdfbdb6b6bc9834699774c9bf777f706d05a88', 'header'),
  },
  success: function() {
    console.log("\n\n\n", client);
    console.log("\n\n\n", client.apis.db.operations.getDbTables);
    console.log("\n\n\n", client.apis.db.operations.getDbTables.operation.parameters);
    client.apis.db.getDbRecords({table_name:"contact"}, function(success){
      console.log('\n\n\nsucceeded and returned this object: ', success.obj);
    });
  },
});
