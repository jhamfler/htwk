#!/bin/bash
if ($# == 0)
then
   echo "usage: ./eps.sh wonderv1/login.mscg"
fi

unamestr=`uname`
if [[ "$unamestr" == 'Linux' ]]; then
	mscg="$1"
	eps=$(echo "$1" | sed -e 's/\.[^\.]\+$//')".eps"
	mscgen -T eps -i "$1" -o "$eps" && evince "$eps"
else
	# probably darwin without xdg-open
	mscg="$1"
        eps=$(echo "$1" | sed -e 's/\.[^\.]\+$//')".eps"
	mscgen -T eps -i "$1" -o "$eps" && open "$eps"
fi

