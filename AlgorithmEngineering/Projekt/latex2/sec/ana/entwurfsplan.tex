\subsection{Entwurfsplan}
\subsubsection{Gründe für das gewählte Entwurfsmuster}
\label{grund-entwurfsmuster}
Anwendungen als \ac{BaaS} zu entwickeln Bedarf eines Konzeptes, welches sich auf die Geschäftslogik der Anwendungsfälle bezieht, welche von dem Service bereit gestellt werden. Daraus ergibt sich, dass eine vorhandene monolithische Anwendung so umgebaut werden muss, dass diese einen Teil ihrer Funktionalität ins Backend und einen Teil ins Frontend verlagert.

Aus sicherheitsrelevanten Gründen sollte deshalb jedwede Autorisierung, Authentifizierung, das Accounting, Verifizierung, Validierung und Fehlererkennung im Backend durchgeführt werden. Eine zusätzliche Prüfung der zu übermittelnden Daten bzw. Schnittstellennutzung im Frontend ist dabei jedoch eine Möglichkeit, um die \ac{UX} zu verbessern und die Last im Backend durch die Fehlerbehebung zu verringern.

Das nachfolgende Konzept in \autoref{fig.entwurfplan} soll dieses Verhalten unter Beachtung der zuvor betrachteten Entwurfsmodelle darstellen. Wie zu erkennen ist, wird für den Entwurfsplan das Entwurfsmuster \ac{MVVM} angewandt.


\begin{figure}[h]
	\centering
	\includegraphics[width=\textwidth]{yed/entwurfplan}
	\caption{Entwurfsplan}
	\label{fig.entwurfplan}
\end{figure}


\subsubsection{Model}
\label{ana.plan.model}
Im Kopf des Models sind die Datenquellen aufgezeigt, welche durch eigene API-Endpunkte freigegeben werden. Die Endpunkte sind notwendig, da einige Datenquellen kein Frontend für Webclients bereitstellen und mit diesem Schritt eine einheitliche Lösung für den Zugriff, wie z.B. REST, möglich gemacht wird. Ein direkter Zugriff auf die Datenquellen würde es zudem nicht ermöglichen, vorab Plausibilitätsprüfungen oder Zugriffsbeschränkungen anzuwenden. Die gezeigten Datenquellen sollen dabei nur beispielhaft sein und können beliebig erweitert werden.

Es ist ersichtlich, dass mehrere API-Endpunkte zu übergeordneten Endpunkten zusammengefasst werden können, jedoch nicht müssen. Je nachdem, ob ein Anwendungsfall mehrere API-Aufrufe nacheinander verlangt oder nicht, soll es mit diesen High-Level-API-Endpunkten möglich sein, eine datenintegritätskritische Operation auszuführen, ohne dabei die Ausfallwahrscheinlichkeit der Verbindung oder die Vertrauensauslagerung zum Client riskieren zu müssen. Diese High-Level-API-Endpunkte sollen weiterhin eine Vereinfachung des Zugriffs auf die Endpunkte der Datenquellen ermöglichen.

Rollen werden genutzt, um Nutzern bestimmte Berechtigungen für den Zugriff auf das Backend einzuräumen. Ob eine Anfrage einer bestimmten Rolle zugeordnet werden kann, entscheidet dabei das \ac{AAA}-System. Es können mehrere Nutzer mehreren Rollen zugeordnet werden, wodurch eine feingranulare Zugriffsberechtigung möglich wird. Die Nutzer sind in der Grafik vernachlässigt worden, da nur deren eingenommene Rollen eine Relevanz für den Entwurfsplan haben.

Es ist eine Analogie zu PAC im Model ersichtlich, wenn die einzelnen Ebenen des Models als PAC-Hierarchiestufen betrachtet werden und wenn davon ausgegangen wird, dass die Datenquellen die obersten Hierarchieebenen des PACs darstellen. Das Model kann jedoch nicht komplett als striktes PAC angesehen werden, da ein PAC eine baumartige Struktur beschreibt, während im Model ein High-Level-Endpunkt mehrere API-Endpunkte nutzen kann und ein Netzwerk entstehen kann. Die Entscheidung das Model nicht als striktes PAC zu beschreiben, ist den oben genannten Gründen geschuldet.

\subsubsection{ViewModel und View}
\label{ana.plan.vmv}
In den Webclients befindet sich das ViewModel und das View, wovon das ViewModel genutzt wird, um die Schnittstelle zum Backend bzw. zum Model zu ermöglichen. Im ViewModel befinden sich die nachgeladenen Funktionen, welche nach der Anmeldung im Backend zum Client transferiert worden sind. Die Funktionen stellen Schnittstellen für den Zugriff auf die API des Backends zur Verfügung. Diese weitere Abstraktionsschicht ermöglicht es, eine Funktion zum Frontend bzw. View bereitzustellen, welche unabhängig vom Model ist, und damit bei einer Veränderung der API keiner Modifikation bedarf. Sollte sich die API im Backend verändern, muss diese nur im ViewModel angepasst werden, während das View nicht beeinflusst wird. Wenn demnach N Views erstellt worden sind, muss nur an 2 anstatt N Stellen der Code verändert werden, um eine Aktualisierung zu erreichen.

Das View wird bei der Initialisierung des Clients vom Backend geholt. In der Grafik ist für die Bereitstellung der Views der API-Endpunkt10 zuständig. Wie zu erkennen ist, darf hierbei das \ac{AAA} nicht greifen, da die Funktionen zur Authentifizierung im Webclient noch nicht vorhanden sind. Bei der Initialisierung müssen demnach öffentlich zugängliche Funktionen geladen werden.

Eine Anwendung, welche für ein BaaS erstellt worden ist, erfüllt generell immer gewisse Funktionalitäten, sodass ein View einem bestimmten Verhalten oder Ablauf folgt. Dieses Verhalten kann, wenn es statisch ist, direkt bei der Initialisierung mit geladen werden. Sollte eine GUI sich dynamisch zu den im Kontext stehenden Informationen verändern, gibt es die Möglichkeit, sämtliche GUI-Elemente bei der Initialisierung mit zu laden und dynamisch auszublenden oder GUI-Elemente vom Backend nachzuladen und diese in den DOM-Baum dynamisch einzufügen. Erstere Option scheint vorteilhaft zu sein, um die Latenz zu verringern, welche ein Nutzer bei der Bedienung der GUI erfährt. Die zweite Option hätte den Vorteil, dass der initiale Aufruf der Webanwendung schneller möglich ist, da weniger Daten geladen werden müssen.

Der Autor empfiehlt, das View komplett bei der Initialisierung der Webanwendung zu laden, da eine Anpassung der Weboberfläche pro Kunde bereits bei der API hinterlegt werden kann. Da das View generell keine veränderlichen Abhängigkeiten zum ViewModel besitzt, ist eine weitere Abstraktion somit nicht nötig.


\subsubsection{Lokalität der Geschäftslogik}
Wenn das Datenmodell der Geschäftslogik in den Client ausgelagert werden soll, muss dieser die Orchestrierung der API-Aufrufe übernehmen. Der Vorteil daraus ist die Entlastung des Backends, da dort lediglich eine Low-Level-API ohne eigene Funktionalität bereitgestellt werden muss, welche Daten nach einer Autorisierung weiterleitet. Der Nachteil der damit entsteht, ist die Verlagerung der Integritätsüberprüfung der Daten in den Client, welcher sicher stellen muss, dass Transaktionen, wie sie in einer Datenbank üblich sind, im Fehlerfall durch einen Rollback wieder hergestellt werden. Wenn bereits eine Datenbank existiert, welche diese Rollbacklogik anwendet, kann dieser Sachverhalt vernachlässigt werden. Sollten jedoch Daten aus verschiedenen Datenbanken oder Dateisystemen voneinander abhängig sein und die Integrität dieser eine Rolle spielen, muss die Ausfallwahrscheinlichkeit und die Korrektheit der Clientfunktionalität sichergestellt werden. Dies ist im allgemeinen nicht möglich, weshalb in dem Fall die Geschäftslogik nicht in den Client verlagert werden darf.


\subsubsection{Modularisierung im Client}

Der Javascriptquelltext im ViewModel, welcher eine Schnittstelle für die GUI bietet, kann anwendungsfallbezogen vom Server geladen werden. So könnten für die Rolle des Autors andere Funktionen als für die Rolle des Administrators heruntergeladen werden, wodurch der Overhead geringer wird und anderen Rollen nicht alle Schnittstellen bekannt sind. Hierfür sind bereits Frameworks etabliert, welche einem die Verwaltungsarbeit im Client für diese Nachladefunktion erleichtern. RequireJS ist geeignet, um sich nicht auf ein Framework für den Client festlegen zu müssen, welches alle Funktionen abzudecken versucht. So wurde zuerst jQuery bevorzugt benutzt, danach AngularJS und in naher Zukunft kann Angular2 als neues Framework prognostiziert werden. Wenn RequireJS genutzt werden sollte, würde sich diese Funktionalität in einem Framework befinden, welches sich genau auf diese spezialisiert hat, wodurch sich die Wiederverwendbarkeit des Codes erhöht.

Die Verwendung von RequireJS hat nicht nur den Vorteil der Spezialisierung einer Funktionalität und den langfristigen Support dieser durch die weniger aufwendige Wartung eines kleinen Frameworks, sondern auch den, dass bestimmte Funktionen wie die Identifizierung eines nachgeladenen Objektes bereits integriert sind. RequireJS kann Module anhand selbst gewählter Namen oder anhand von deren Herkunft identifizieren und somit Duplikate vermeiden und den Overhead bei einem erneuten Aufruf verringern.

Nachfolgende Tabelle zeigt die Codegröße der minifizierten Bibliotheken und Frameworks im Vergleich.

\begin{table}[h]
	\centering
	\begin{tabular}{l|l|r}
		Bibliothek & Version & Größe in Bytes \\
		\hline
		RequireJS  & 2.3.2   & 17.623  \\
		AngularJS  & 1.5.8   & 159.921 \\
		jQuery     & 3.1.1   & 86.643  \\
	\end{tabular}
	\caption{Codegröße verschiedener Bibliotheken}
	\label{tab.frameworks}
\end{table}

Wie zu erkennen ist, benötigt RequireJS sowohl im Browser als auch bei der Übertragung nur wenig Platz. Auch wenn jQuery und AngularJS Funktionen zum Nachladen von Code beinhalten, muss man sich auf ein Framework festlegen, um die Anwendungen verständlich und wartbar zu halten. Da immer mehr Frameworks entstehen und nicht mit absoluter Sicherheit gesagt werden kann, welches dieser in Zukunft das beste für einen Anwendungsfall wird, sind die zusätzlichen 18kB im Verhältnis zur Wartbarkeit und Zukunftssicherheit gerechtfertigt.
%
%Mit Hilfe von Javascript kann seit geraumer Zeit Code nachgeladen werden, was der Verwendung einer eigenständigen Bibliothek für diesen Zweck entgegen steht. Die nachfolgenden Abschnitte werden diese Diskussion führen und versuchen die Schwächen einer manuellen Variante aufzuzeigen.
%
%Javascript kann mittels
%\ac{AJAX}-Requests\footnote{Als \ac{AJAX}-Request wird die Nutzung des 
%	\texttt{XMLHttpRequest}-Objektes zur Kommunikation mit serverseitigen Skripten 
%	bezeichnet\cite{w3c:xmlhttprequest}. \ac{AJAX} ermöglicht es, Information nach 
%	dem Laden einer Website abzurufen und durch \ac{DOM}-Manipulation hinzuzufügen.} 
%in den \ac{HTML}-\ac{DOM}\footnote{Das \acl{DOM} bildet die 
%	Schnittstelle zwischen \ac{HTML} und JavaScript.} geladen werden \cite{w3c:xml,gh:wonder}.
%Das Nachladen geschieht über eine Funktion, die für diesen Zweck konzipiert 
%wurde und in \autoref{lst:loadjsfile} zu sehen ist.
%
%In diesem Listing wird ein \ac{HTML}-Script-Tag erstellt, mit Attributen 
%versehen
%und anschließend in den Head-Bereich des \ac{DOM}-Baumes geschrieben 
%\cite{w3c:html5}(Abschnitt 4.11).
%Da der Script-Tag ein \texttt{src}-Element mit einem \ac{URI} enthält,
%lädt der Browser automatisch die in dem \ac{URI} hinterlegte JavaScript-Datei 
%nach.
%Diese Variante des Nachladens hat das Problem, dass das \ac{DOM}-Element 
%manipuliert wird,
%was zu Namenskonflikten beim Nachladen führen kann \cite{dyn-laden}. Sollte eine
%Funktion den gleichen Namen wie eine zweite haben, so wird die zweite Funktion die
%erste überschreiben, wenn sie später nachgeladen wird. Funktionen dürften demnach
%nicht den gleichen Namen besitzen, weshalb für diese Variante eine
%Standardisierung für die Namensvergabe notwendig ist.
%Des Weiteren werden nachgeladene JavaScript-Dateien nicht indiziert,
%wodurch sie später nicht gelöscht werden können, da keine Referenz auf sie 
%vorhanden ist.
%Dies erhöht die Speicherauslastung der Anwendung, je mehr Funktionen nachgeladen werden.
%
%\lstinputlisting[language=javascript, caption={Nachladefunktion in ES5}, 
%label=lst:loadjsfile]{src/js/loadjsfile.js}
%
%Es wird ebenfalls ein Timer benötigt, welcher prüft, ob eine Funktion
%zu einer gegebenen Zeit nachgeladen wurde. Der Aufruf und dessen Funktion sind in 
%\autoref{lst:callloadjsfile} zu sehen.
%
%\lstinputlisting[language=javascript, caption={Aufruf der Nachladefunktion}, 
%label=lst:callloadjsfile]{src/js/callloadjsfile.js}
%
%Wie zu erkennen ist, sind mit dieser Methode nur Intervalle für die Abfrage des Vorhandenseins der nachgeladenen Funktion möglich. Selbst mit einem Feintuning der Funktion auf die zu erwartende Latenz des Netzwerks, ist dieses Verfahren zu statisch. Ebenfalls problematisch ist die Unterbrechung der gesamten clientseitigen Anwendung durch den Timer, welcher den Programmfluss blockiert. 
%Auch wenn diese Methode des Nachladens vor einer Zeit noch akzeptabel war, ist sie aus heutiger Sicht nicht tragbar. Abhilfe kann an dieser Stelle die Verwendung des neueren ES6-Standards schaffen, welcher Promises für asynchrone Aufrufe ermöglicht, wodurch die Auflösung des Erfolgs der Funktion ebenfalls asynchron stattfindet. Der Programmfluss muss somit nicht unterbrochen werden und die Erfolgsbehandlung des Nachladens erfolgt so zeitnah wie möglich.
%
%
%\lstinputlisting[language=javascript, caption={Nachladen von Dateien mittels 
%	Require.js}, 
%label=lst:requireaufruf]{src/js/requireaufruf.js}


Die Nutzung von Require.js bietet den Vorteil, dass angeforderte Dateien 
unabhängig 
von ihren Namen geladen werden. Sollten somit Funktionen mit dem gleichen Namen 
geladen werden, überschreiben diese sich nicht gegenseitig und können 
unabhängig voneinander genutzt werden. Die Identifizierung der angeforderten 
Funktionen durch Require.js erfolgt dabei über den Herkunftsort der Datei. 
Weiterhin sorgt Require.js dafür, dass angeforderte Dateien im Client 
gecached
%\footnote{Cachen bezeichnet das Zwischenspeichern von Informationen.} 
werden. Somit müssen diese bei erneuter Anforderung nicht über das Netzwerk bezogen werden. Dadurch kann der Overhead minimiert und die Reaktionsfähigkeit der Anwendung erhöht werden.

Wenn davon ausgegangen wird, dass in Zukunft das Framework nicht ausgetauscht wird, sind die genannten Argumente nichtig. So kann AngularJS über die eigene Nachladefunktion die im Client verfügbare Codebasis erweitern. Gleiches gilt für Angular2.


