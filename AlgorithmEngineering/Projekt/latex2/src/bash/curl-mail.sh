curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' --header 'X-DreamFactory-Api-Key: d7e617f86d10ace38ff5c73b8d770e039e274d4b783c2278d448520c24b130b5' --header 'X-DreamFactory-Session-Token: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsInVzZXJfaWQiOjEsImVtYWlsIjoiZGZAejdrLmRlIiwiZm9yZXZl ciI6ZmFsc2UsImlzcyI6Imh0dHA6XC9cL2xvY2FsaG9zdDo4MDgwXC9hcGlcL3Yy XC9zeXN0ZW1cL2FkbWluXC9zZXNzaW9uIiwiaWF0IjoxNDc2MTAxMjI0LCJleHAi OjE0NzYxMDQ4MjQsIm5iZiI6MTQ3NjEwMTIyNCwianRpIjoiNmE4M2Y3YWRiZTFk MGYyOGE1M2M4MTlmYmMxNzc1Y2MifQ.a9OqsulYmkN80NOVpuPmJjFs5G-Q07ebarCQxDFAZH8' --header 'Authorization: Basic amhAejdrLmRlOmRyZWFtZmFjdG9yeTI3' -d '{
  "to": [
    {
      "name": "dfmailtest",
      "email": "testdfmail@googlemail.com"
    }
  ],
  "subject": "thema der mail",
  "body_text": "inhalt der mail"
}' 'http://localhost:8080/api/v2/amsysmail'
