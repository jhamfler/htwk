var lodash = require("lodash.min.js");
 
if (event.request.method !== "GET") {
    throw("Only HTTP GET is allowed on this service.");
}

if (event.resource == "") {
    throw("no endpoint");
}
 
switch (event.resource) {
    case "congressusers":
        user = platform.api.get("user/session")
        amsysuser = platform.api.get("mysql/_table/bitnami_dreamfactory.amsysuser?ids="+user.content.id);
        amsysuser = amsysuser.content.resource[0]; // nur ein Wert moeglich
        congressusers = platform.api.get("mysql/_table/bitnami_dreamfactory.amsysuser?filter=congressid%3D"+Number(amsysuser.congressid));
        result = {"resource": [amsysuser, congressusers, event.request, user]};
        break;
}
 
return result;
