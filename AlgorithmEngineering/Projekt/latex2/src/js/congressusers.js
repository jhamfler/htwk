var lodash = require("lodash.min.js");
 
if (event.request.method !== "GET") {
    throw("Only HTTP GET is allowed on this service.");
}

if (event.resource == "") {
    throw("no endpoint");
}
 
switch (event.resource) {
    case "congressusers":
        amsysusers = platform.api.get("mysql/_table/bitnami_dreamfactory.amsysuser/");
        amsysusers = amsysusers.content.resource; // array
        user = platform.api.get("user/session")
        amsysuser=null;
        for (i=0; i<amsysusers.length; i++){
            if (amsysusers[i].id == user.content.id){
                amsysuser = amsysusers[i];
                break;
            }
        }
        congressusers = [];
        for (i=0; i<amsysusers.length; i++){
            if (amsysusers[i].congressid == amsysuser.congressid){
                congressusers.push(amsysusers[i]);
            }
        }
        result = {"resource": [amsysuser, amsysusers, congressusers, event.request, user]};
        
        break;
}
 
return result;
