/**
 * Created by Christopher Rost on 23.10.2014.
 * christopher.rost@hft-leipzig.de
 * (c) Christopher Rost
 */

(function () {
    'use strict';

    angular
        .module('userSpecificModule')
        .service('weatherService', weatherService);

    weatherService.$inject = [
        '$http'
    ];

    var weatherInfos = [];
    var weatherSearch = [];

    function weatherService($http) {
        return {
            getWeatherByCodeFromServer: _getWeatherByCodeFromServer,
            getWeather: _getWeather,
            makeWeatherSearch: _makeWeatherSearch,
            getSearchResults: _getSearchResults
        };

        function _getWeatherByCodeFromServer(locationCode) {
            var projectname = "userspecificwebapp";
            var api_key = "5d53ec5c68f216c57baf0d20bb177b23";
            var checksum = CryptoJS.MD5(projectname + api_key + locationCode);
            var url = "http://api.wetter.com/forecast/weather/city/" + locationCode + "/project/" + projectname + "/cs/" + checksum + "/output/json?jsoncallback=JSON_CALLBACK";

            $http.jsonp(url).success(function (responseData) {
                //console.log(responseData);
                weatherInfos[locationCode] = responseData;
            });
        }

        function _getWeather() {
            return weatherInfos;
        }

        function _makeWeatherSearch(searchString) {
            var projectname = "userspecificwebapp";
            var api_key = "5d53ec5c68f216c57baf0d20bb177b23";
            var checksum = CryptoJS.MD5(projectname + api_key + searchString);
            var url = "http://api.wetter.com/location/name/search/" + searchString + "/project/" + projectname + "/cs/" + checksum + "/output/json?jsoncallback=JSON_CALLBACK";

            $http.jsonp(url).
                success(function (data, status, headers, config) {
                    //console.log("Get Weather Search successful: \nData: " + data + "\nStatus: " + status);
                    weatherSearch = data;
                }).
                error(function (data, status, headers, config) {
                    console.log("Get Weather Search error: \nData: " + data + "\nStatus: " + status);
                });
        }

        function _getSearchResults() {
            return weatherSearch;
        }

    }

}());

