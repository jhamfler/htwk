/**
 * Created by Christopher Rost on 23.10.2014.
 * christopher.rost@hft-leipzig.de
 * (c) Christopher Rost
 */

(function () {
    'use strict';

    angular
        .module('userSpecificModule')
        .service('pageService', pageService);

    pageService.$inject = [
        '$http',
        '$location'
    ];

    var pages = [];
    var pagesWithKeyId = [];
    var contents = [];
    var activePageId = 0;
    var updateTextSuccessful = false;

    function pageService($http, $location){
        return{
            getPagesFromServer:_getPagesFromServer,
            getPages:_getPages,
            getPageById:_getPageById,
            getContentsByPageIdFromServer:_getContentsByPageIdFromServer,
            getContentsByPageId:_getContentsByPageId,
            getContentsForActivePage:_getContentsForActivePage,
            getActivePageId:_getActivePageId,
            getUpdateTextSuccessfulState:_getUpdateTextSuccessfulState,
            deleteContentByContentIdAtActivePage:_deleteContentByContentIdAtActivePage,
            deletePageById:_deletePageById,
            setActivePageId:_setActivePageId,
            addNewPage:_addNewPage,
            addNewContent:_addNewContent,
            updateTextByContentIdAtActivePage:_updateTextByContentIdAtActivePage
        };

        function _getPagesFromServer(userId) {
            var url = "http://localhost:8080/rest/users/" + userId + "/pages/";
            $http.get(url).
                success(function(data, status, headers, config){
                    pages = data;
                    pages.forEach(function(page){
                        var id = page._id;
                        pagesWithKeyId[id] = page;
                    });
                }).
                error(function(data, status, headers, config){
                    console.log("Get Page error: \nData: " + data + "\nStatus: " + status);
                });
        }

        function _getPages(){
            return pages;
        }

        function _getPageById(pageId){
            return pagesWithKeyId[pageId];
        }

        function _getContentsByPageIdFromServer(userId, pageId){
            var url = "http://localhost:8080/rest/users/" + userId + "/pages/" + pageId +"/contents/";
            $http.get(url).
                success(function(data, status, headers, config) {
                    data.forEach(function (content){
                        if(content.ContentType == 'text'){
                            var encoded = atob(content.ContentOption);
                            content.ContentOption = encoded;
                        }
                    });
                    contents[pageId] = data;
                }).
                error(function(data, status, headers, config) {
                    console.log("Get Content error: \nData: " + data + "\nStatus: " + status);
                });
        }

        function _getContentsByPageId(pageId){
            return contents[pageId];
        }

        function _getContentsForActivePage(){
            return contents[activePageId];
        }

        function _deleteContentByContentIdAtActivePage(userId, contentId){
            var url = "http://localhost:8080/rest/users/" + userId + "/pages/" + activePageId +"/contents/" + contentId;
            $http.delete(url).
                success(function(data, status, headers, config) {
                    //console.log("Delete Content successful: \nData: " + data + "\nStatus: " + status);
                    _getContentsByPageIdFromServer(userId,activePageId);
            }).
                error(function(data, status, headers, config) {
                    console.log("Delete Page error: \nData: " + data + "\nStatus: " + status);
                });
        }

        function _getActivePageId(){
            return activePageId;
        }

        function _setActivePageId(pageId){
            activePageId = pageId;
            updateTextSuccessful = false;
        }

        function _addNewPage(userId, pageName){
            var message = '{"PageName":"' + pageName + '"}';
            $http.post("http://localhost:8080/rest/users/" + userId + "/pages/", message).
                success(function(data, status, headers, config) {
                    //console.log("AddNewPage Post successful: \nData: " + data + "\nStatus: " + status);
                    $location.path('/page/' + data._id);
                    _getPagesFromServer(userId);
                }).
                error(function(data, status, headers, config) {
                    console.log("AddNewPage Post error: \nData: " + data + "\nStatus: " + status);
                });

        }

        function _deletePageById(userId,pageId){
            $http.delete("http://localhost:8080/rest/users/" + userId + "/pages/" + pageId).
                success(function(data, status, headers, config) {
                    //console.log("Delete Page successful: \nData: " + data + "\nStatus: " + status);
                    _getPagesFromServer(userId);
                    $location.path('/home');
                }).
                error(function(data, status, headers, config) {
                    console.log("Delete Page error: \nData: " + data + "\nStatus: " + status);
                });
        }

        function _addNewContent(userId,pageId,contentTitle,contentType,contentOption){
            var message = '{"Title":"' + contentTitle + '","ContentType":"' + contentType + '","ContentOption":"' + contentOption + '"}';
            var url = "http://localhost:8080/rest/users/" + userId + "/pages/" + pageId + "/contents/";
            $http.post(url , message).
                success(function(data, status, headers, config) {
                    //console.log("AddNewContent Post successful: \nData: " + data + "\nStatus: " + status);
                    _getContentsByPageIdFromServer(userId,pageId);
                }).
                error(function(data, status, headers, config) {
                    console.log("AddNewContent Post error: \nData: " + data + "\nStatus: " + status);
                });

        }

        function _updateTextByContentIdAtActivePage(userId,pageId , contId,text){
            var textB64 = '';
            textB64 = btoa(text);
            updateTextSuccessful = false;
            var url = "http://localhost:8080/rest/users/" + userId + "/pages/" + pageId + "/contents/" + contId;
            var message = '{"_id":"' + contId + '","Title":"TextContent","ContentType":"text","ContentOption":"' + textB64 + '","PID":"' + pageId + '"}';
            $http.post(url , message).
                success(function(data, status, headers, config) {
                    //console.log("Update Content Post successful: \nData: " + data + "\nStatus: " + status);
                    _getContentsByPageIdFromServer(userId,pageId);
                    updateTextSuccessful = true;
                }).
                error(function(data, status, headers, config) {
                    console.log("Update Content Post error: \nData: " + data + "\nStatus: " + status);
                });
        }

        function _getUpdateTextSuccessfulState(){
            return updateTextSuccessful;
        }
    }

})();