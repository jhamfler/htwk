/**
 * Created by Christopher Rost on 23.10.2014.
 * christopher.rost@hft-leipzig.de
 * (c) Christopher Rost
 */

(function () {
    'use strict';

    angular
        .module('userSpecificModule')
        .service('userService', userService);

    userService.$inject = ['$http','pageService'];

    var activeUserId = '';
    var userLoggedIn = false;
    var users = [];
    var activeUser = [];

    function userService($http,pageService){
        return{
            getUserLoggedIn:_getUserLoggedIn,
            setUserLoggedIn:_setUserLoggedIn,
            setUserLoggedOut:_setUserLoggedOut,
            getActiveUserId:_getActiveUserId,
            setActiveUserId:_setActiveUserId,
            getUsersFromServer:_getUsersFromServer,
            checkUser:_checkUser,
            updateUserPic:_updateUserPic
        };

        function _getUserLoggedIn(){
            return userLoggedIn;
        }

        function _setUserLoggedIn(){
            userLoggedIn = true;
        }

        function _setUserLoggedOut(){
            userLoggedIn = false;
        }

        function _getActiveUserId(){
            return activeUserId;
        }

        function _setActiveUserId(id){
            if(!isNaN(id)) {
                activeUserId = id;
            }
        }

        function _getUsersFromServer() {
            var url = "http://localhost:8080/rest/users";
            $http.get(url).
                success(function(data, status, headers, config){
                    users = data;
                }).
                error(function(data, status, headers, config){
                    console.log("Get Users Fehlgeschlagen: " + status);
                });
        }

        function _checkUser(inputName){
            users.forEach(function(user){
                if(user.UserName == inputName){
                    //console.log("User " + user.UserName + " gefunden!");
                    activeUserId = user._id;
                    userLoggedIn = true;
                    //console.log(user + " mit der ID:" + activeUserId + " hat sich eingeloggt");
                    pageService.getPagesFromServer(user._id);
                    activeUser = user;
                }
            });
            return activeUser;
        }

        function _updateUserPic(picUrl){
            var message = '{ "_id" : "' + activeUserId + '" , "UserName" : "' + activeUser.UserName + '" , "PreName" : "' + activeUser.PreName + '" , "LastName" : "' + activeUser.LastName + '" , "PicURL" : "' + picUrl + '"}';
            var url = "http://localhost:8080/rest/users/" + activeUserId + "/";
            $http.post(url , message).
                success(function(data, status, headers, config) {
                    //console.log("Update User Post successful: \nData: " + data + "\nStatus: " + status);
                }).
                error(function(data, status, headers, config) {
                    console.log("Update User Post error: \nData: " + data + "\nStatus: " + status);
                });

        }
    }

})();