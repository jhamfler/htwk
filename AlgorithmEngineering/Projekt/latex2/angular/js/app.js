/**
 * Created by Christopher Rost on 23.10.2014.
 * christopher.rost@hft-leipzig.de
 * (c) Christopher Rost
 */

(function () {
    'use strict';

    angular
        .module('userSpecificModule',[
            'ngAnimate',
            'ngRoute',
            'ngResource',
            'mgcrea.ngStrap',
            'ui.bootstrap.tpls',
            'ui.bootstrap.popover'
        ])
        .config(defineRoutes);

    defineRoutes.$inject = ['$routeProvider'];

    function defineRoutes($routeProvider){
        $routeProvider
            .when('/',  { templateUrl: './view/home.html', controller: "pageCtrl"})
            .when('/home', { templateUrl: './view/home.html', controller: "pageCtrl"})
            .when('/page/:pageid', { templateUrl: './view/bodyPage.html', controller: "contentCtrl" })
            .otherwise({ redirectTo: '/' });
    }

})();




