/**
 * Created by Christopher Rost on 23.10.2014.
 * christopher.rost@hft-leipzig.de
 * (c) Christopher Rost
 */

(function(){
    'use strict';

    angular
        .module('userSpecificModule')
        .controller('pageCtrl', pageCtrl);

    pageCtrl.$inject = [
        '$scope',
        '$location',
        'pageService',
        'userService'
    ];

    function pageCtrl($scope, $location, pageService, userService){
        /* Scope-Variablen */
        $scope.data = {};
        $scope.data.activeUserId = userService.getActiveUserId();
        $scope.data.loggedIn = userService.getUserLoggedIn();
        $scope.data.username = '';
        $scope.data.showRemovePageButton = false;
        $scope.data.loginBtnClass = 'form-group';
        $scope.pages = [];
        $scope.pages2 = [];
        $scope.data.user = {};
        $scope.data.profilePicURL = '';
        $scope.data.newPageName = '';
        $scope.myPageService = pageService;

        /* Watch Funktion */
        $scope.$watch('myPageService.getPages()', function(newValue, oldValue){
            if(newValue != oldValue){
                //console.log("Watch wurde ausgeführt");
                $scope.pages = newValue;
                if($scope.pages.length){
                    $scope.data.showRemovePageButton = true;
                }else $scope.data.showRemovePageButton = false;
                //Hole den Content von allen Seiten
                for(var index = 0 ; index < $scope.pages.length ; index ++){
                    pageService.getContentsByPageIdFromServer(userService.getActiveUserId(),$scope.pages[index]._id);
                }

            }
        });

        /* Methoden */
        $scope.onLogoutBtnClick = function(){
            userService.setUserLoggedOut();
            $scope.loggedIn = userService.getUserLoggedIn();
            $location.path('/');
            window.location.reload();
        };

        $scope.onLoginBtnClick = function(){
            $scope.data.loginBtnClass = 'form-group has-error';
            $scope.data.user = userService.checkUser($scope.data.username);
            $scope.data.loggedIn = userService.getUserLoggedIn();
            $scope.data.activeUserId = userService.getActiveUserId();
            //console.log($scope.data.user);
        };

        $scope.onAddPageClick = function(){
            pageService.addNewPage(userService.getActiveUserId(),$scope.data.newPageName);
        };

        $scope.onRemovePageClick = function(){
            pageService.deletePageById(userService.getActiveUserId(),pageService.getActivePageId());
        };

        $scope.getActivePageName = function(){
            var tempPage = pageService.getPageById(pageService.getActivePageId());
            if(tempPage){
                return tempPage.PageName;
            }else return 'undefined';
        };

        $scope.onProfilPicChangeClick = function(){
            userService.updateUserPic($scope.data.profilePicURL);
            $scope.data.user.PicURL = $scope.data.profilePicURL;
        }
    }
})();
