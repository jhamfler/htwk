/**
 * Created by Christopher Rost on 23.10.2014.
 * christopher.rost@hft-leipzig.de
 * (c) Christopher Rost
 */

(function () {
    angular
        .module('userSpecificModule')
        .directive('loginBar', loginBar);

    function loginBar(){
        return {
            restrict: 'E',
            templateUrl: 'view/templates/loginForm.tpl.html',
            controller: 'pageCtrl'
        };
    }
}());