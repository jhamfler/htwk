/**
 * Created by Christopher Rost on 23.10.2014.
 * christopher.rost@hft-leipzig.de
 * (c) Christopher Rost
 */

(function () {
    angular
        .module('userSpecificModule')
        .directive('navBar', navBar);

    function navBar(){
        return {
            restrict: 'E',
            templateUrl: 'view/templates/navbar.tpl.html',
            controller: 'pageCtrl'
        };
    }
}());