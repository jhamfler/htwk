/**
 * Created by Christopher Rost on 23.10.2014.
 * christopher.rost@hft-leipzig.de
 * (c) Christopher Rost
 */

(function(){
    'use strict';

    angular
        .module('userSpecificModule')
        .controller('contentCtrl', contentCtrl);

    contentCtrl.$inject = [
        '$scope',
        '$routeParams',
        'weatherService',
        'pageService',
        'userService'
    ];

    function contentCtrl($scope, $routeParams, weatherService, pageService, userService){
        /* Scope Variablen */
        $scope.pageid = $routeParams.pageid;
        $scope.data.activePage = '';
        $scope.data.picChecked = 0;
        $scope.data.radioSelect = '';
        $scope.data.weather = {};
        $scope.data.weather.inputLocation = '';
        $scope.data.weather.tempResults = [];
        $scope.data.weather.decision = '';
        $scope.data.date = new Date();
        $scope.data.saveTextButtonClass = 'btn-primary';
        $scope.page = {};
        $scope.page.contents = [];
        $scope.myPageService = pageService;
        $scope.myWeatherService = weatherService;

        /* Watch Funktionen */
        $scope.$watch('myPageService.getContentsForActivePage()', function(newValue, oldValue){
            if(newValue != oldValue){
                //console.log("Watch wurde ausgeführt für Contents");
                $scope.page.contents = newValue;
            }
        });

        $scope.$watch('myWeatherService.getSearchResults()', function(newValue, oldValue){
            if(newValue != oldValue){
                $scope.data.weather.tempResults = newValue.search.result;
            }
        });

        $scope.$watch('myPageService.getUpdateTextSuccessfulState()', function(newValue, oldValue){
            if(newValue != oldValue){
                if(newValue){
                    $scope.data.saveTextButtonClass = 'btn-success';
                }
            }
        });

        /* Ausführung nach Laden des Controllers */
        pageService.setActivePageId($scope.pageid);
        $scope.data.activePage = pageService.getActivePageId();

        $scope.page.contents = pageService.getContentsForActivePage();

        //Suche nach einem Wetter Content und hole die Info vom Server
        if($scope.page.contents){
            for(var index = 0; index < $scope.page.contents.length ; index++){
                if($scope.page.contents[index].ContentType === 'weather'){
                    weatherService.getWeatherByCodeFromServer($scope.page.contents[index].ContentOption);
                }
            }
        }

        /* Methoden */
        $scope.onAddContentClick = function(){
            var contentTitle = '';
            var contentType = '';
            var contentOption = '';

            switch($scope.data.radioSelect){
                case 'text':
                    //console.log("Neuer Content: Text auf Seite " + $scope.data.activePage + " von User " + userService.getActiveUserId());
                    contentTitle = 'TextContent';
                    contentType = 'text';
                    contentOption = '';
                    break;
                case 'weather':
                    //console.log("Neuer Content: Wetter für " + $scope.data.weather.decision + " auf Seite " + $scope.data.activePage + " von User " + userService.getActiveUserId());
                    contentTitle = 'WeatherContent';
                    contentType = 'weather';
                    contentOption = $scope.data.weather.decision;
                    weatherService.getWeatherByCodeFromServer(contentOption);
                    break;
                case 'picture':
                    //console.log("Neuer Content: Picture (" + $scope.data.picChecked + ") auf Seite " + $scope.data.activePage + " von User " + userService.getActiveUserId());
                    contentTitle = 'PictureContent';
                    contentType = 'picture';
                    contentOption = 'img/pic_00' + $scope.data.picChecked + '.jpg';
                    break;
            }
            pageService.addNewContent(userService.getActiveUserId(),pageService.getActivePageId(),contentTitle,contentType,contentOption);

        };

        $scope.remContent = function(contId){
            pageService.deleteContentByContentIdAtActivePage($scope.data.activeUserId,contId);
        };

        $scope.setPicChecked = function(picNr){
            $scope.data.picChecked = picNr;
        };

        $scope.makeWeatherSearch = function(){
            weatherService.makeWeatherSearch($scope.data.weather.inputLocation);
        };

        $scope.inputTextChanged = function(contId){
            var text = '';
            $scope.data.saveTextButtonClass = 'btn-warning';
            $scope.page.contents.forEach(function(content){
                if(content._id === contId){
                    text = content.ContentOption;
                }
            });
            pageService.updateTextByContentIdAtActivePage(userService.getActiveUserId(), pageService.getActivePageId(), contId,text );
        };
    }

})();