/**
 * Created by Christopher Rost on 23.10.2014.
 * christopher.rost@hft-leipzig.de
 * (c) Christopher Rost
 */

(function(){
    'use strict';

    angular
        .module('userSpecificModule')
        .controller('IndexCtrl', IndexCtrl);

    IndexCtrl.$inject = [
        '$scope',
        'userService'
    ];

    function IndexCtrl($scope, userService){
        $scope.loggedIn = false;
        $scope.title = "UserSpecificWebApp";

        userService.getUsersFromServer();

        $scope.isActive = function (viewLocation) {
            var active = (viewLocation === $location.path());
            return active;
        };
    }

})();
