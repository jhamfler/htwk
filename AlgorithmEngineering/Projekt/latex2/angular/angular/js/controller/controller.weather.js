/**
 * Created by Christopher Rost on 23.10.2014.
 * christopher.rost@hft-leipzig.de
 * (c) Christopher Rost
 */

(function(){
    'use strict';

    angular
        .module('userSpecificModule')
        .controller('weatherCtrl', weatherCtrl);

    weatherCtrl.$inject = [
        '$scope',
        'weatherService'
    ];

    function weatherCtrl($scope, weatherService){
        $scope.data.weather.info = weatherService.getWeather();
        var dt = new Date();
        var tag = '';
        var monat = '';
        tag = dt.getDate();
        monat = dt.getMonth() + 1;
        if(tag < 10) tag = "0" + tag;
        if(monat < 10) monat = "0" + monat;
        $scope.today = dt.getFullYear() + "-" + monat + "-" + tag;
    }

})();