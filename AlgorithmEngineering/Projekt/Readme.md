# Effizienz von Mehrfachmustererkennungsalgorithmen in der Textsuche

Das Projekt ist entstanden, um die Effizienz der Suche mit mehreren Suchbegriffen zwischen einer primitive Methode, Rabin-Karp und Wu-Manber zu vergleichen.

Führe aus:

```
make
make mtest
```

Einstiegspunkt in den Programmablauf ist allmtest.sh
Alle anderen Skripte haben die Funktion, die ihr Name preisgibt oder sind Überbleibsel von Hilfsprogrammen, um Parameter zu bestimmen.

Die Dokumentation ist unter [Dokumentation](latex2/AE_Hamfler,Johannes.pdf) zu finden. Die Betrachtung im Browser wird nicht empfohlen, da die Seiten verschoben sein können und die Anzahl der Messpunkte die Anzeige verlangsamt.