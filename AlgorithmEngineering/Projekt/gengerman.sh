#!/bin/bash
wortdatei=/usr/share/dict/ngerman

if [ $# -ne 1 ]
then
	echo "Please specify how many random words would you like to generate !" 1>&2 
	echo "example: ./random-word-generator 3" 1>&2 
	echo "This will generate 3 random words" 1>&2 
	exit 0
fi

i=0
anzahl=$(wc -l $wortdatei)

while (( $i < $1 ))
do
	nummer=$(od -N3 -An -i /dev/urandom | awk -v a="$anzahl" '{printf "%i\n", a * $1 / 16777216}')
	sed -n "$nummer""p" $wortdatei
	i=$(($i+1))
done
