#!/bin/bash

# messe den punkt, ab welcher anzahl gleichzeitiger suchbegriffe rk schneller als primitiv ist
DEBUG=1
INFO=1
pinit=2

declare -a algorithmen=("rkm" "wmm" "pm")
#declare -a algorithmen=("rkm" "wmm")
#declare -a algorithmen=("p2")
suchdatenort=txt/
anzahlsuchbegriffe=("5" "10" "15" "20") #20
# latex erwähnt
anzahlmessreihen=1 #20
messzielpfad=messwerte/multiplesuchbegriffe/

# latex erwähnt
#suchdaten=gen # a-z 20-240 mb
#suchdaten=gen2 # a-b 12-290 mb
#suchdaten=gen3 # a-z 100-2500 kb
#suchdaten=science
suchdaten=alle # 2Gb Textdateien
#suchdaten=txt

suchbegrifflen=5
#alphabet=gen
#alphabet=ngerman
alphabet=american-english
wortdatei=/usr/share/dict/$alphabet
wortanzahl=$(cat $wortdatei | wc -l)


mkdir -p $messzielpfad"mittelwert/"
# messen
for anzahl in ${anzahlsuchbegriffe[@]}
do
	if (( $INFO > 0 )); then echo "Anzahl Suchbegriffe $anzahl"; fi
	# kopf für messwertdatei anlegen
	for algo in ${algorithmen[@]}
	do
		echo "Targetsize    Time" > $messzielpfad$algo.$anzahl.$suchdaten
	done

	# Messreihen durchgehen
    for i in $(seq 1 $anzahlmessreihen)
    do
		if (( $INFO > 0 )); then echo "Messreihe $i"; fi
		# generiere immer neue Suchdaten, damit die Suchwörter die Mittelwerte nicht beeinflussen
		suchbegriffe=()
		for i in $(seq 1 $anzahl)
		do
			# wähle korrektes Wörterbuch aus
			if [[ "$alphabet" == "gen" ]]
			then
				suchbegriffe+=("$(base64 /dev/urandom | grep '[[:alpha:]]*' -o | grep '[[:lower:]]' -o | tr -d '\n' | head -c $(($suchbegrifflen)))")
			else
				nummer=$(od -N3 -An -i /dev/urandom | awk -v a="$wortanzahl" '{printf "%i\n", a * $1 / 16777216}')
				suchbegriffe+=("$(sed -n ${nummer}p $wortdatei)")
			fi
		done
		if (( $INFO > 0 )); then echo "Suchbegriffe ${suchbegriffe[@]}"; fi

		# zu durchsuchenden Text durchgehen
		for file in $suchdatenort$suchdaten/*
		do
			# jeden Algorithmus auf jeden zu durchsuchden Text anwenden
			#p=$pinit # 0010 1010 - hex 2, 8 und 20 #zum Verteilen der Prozesse auf die Kerne 1, 3 und 5 von 0-7 -> jeder Prozess hat einen echten Kern
			for algo in ${algorithmen[@]}
			do
	       	    if (( $DEBUG > 0 )); then echo "./$algo $file ${suchbegriffe[@]} >> $messzielpfad$algo.$anzahl.$suchdaten"; fi
				# weise Prozess jeweiligem Kern zu
				#taskset $(printf "%X" $(($p*2))) 
				./$algo "$file" ${suchbegriffe[@]} >> $messzielpfad$algo.$anzahl.$suchdaten
				#pids="$pids $!"
				#p=$(($p*4)) # p<<2
			done
			#wait # warte bis alle Prozesse beendet wurden bevor neue gestartet werden
			# rotiere Algorithmen -> falls ein Kern höher belastet ist als andere gleicht eine Umverteilung die ungleichen Bedingungen aus
			#algo=("${algo[@]: -1}" "${algo[@]:0:${#algo[@]}-1}")
       	done
	done

    # mittelwerte berechnen
	if (( $INFO > 0 )); then echo "Mittelwerte werden berechnet"; fi
	pids=""
	for algo in ${algorithmen[@]}
	do
		if (( $DEBUG > 1 )); then echo "./mw $messzielpfad$algo.$anzahl.$suchdaten > $messzielpfad"mittelwert/"$algo.$anzahl.$suchdaten"; fi
		./mw $messzielpfad$algo.$anzahl.$suchdaten > $messzielpfad"mittelwert/"$algo.$anzahl.$suchdaten
		pids="$pids $!"
	done
	wait $pids
done



zielort=$messzielpfad"mittelwert/"
divplotdatei=$zielort"divplot.r"
algos=("rkm" "wmm") # rkm / wmm

for anzahl in ${anzahlsuchbegriffe[@]}
do
	./div $messzielpfad"mittelwert/"${algos[0]}.$anzahl.$suchdaten $messzielpfad"mittelwert/"${algos[1]}.$anzahl.$suchdaten > $messzielpfad"mittelwert/"${algos[0]}"-"${algos[1]}.$anzahl.$suchdaten.div

    # r skript vorbereiten
    echo "${algos[0]}${algos[1]} = read.table"'("/home/jh/git/htwk/AlgorithmEngineering/Projekt/'"$zielort"${algos[0]}-${algos[1]}.$suchdaten.div'"'', header = TRUE)' >> $divplotdatei
    echo 'pdf("/home/jh/git/htwk/AlgorithmEngineering/Projekt/graphen/div'.${algos[0]}-${algos[1]}."$suchdaten"'.pdf")' >> $divplotdatei
    echo "plot(${algos[0]}${algos[1]}"'$Time ~ '"${algos[0]}${algos[1]}"'$Targetsize, '"${algos[0]}${algos[1]}, main = "'"Methode: '${algos[0]}"/"${algos[1]}'", sub="Suchbegriffanzahl '"$anzahl"'", xlab = "Zielgröße", ylab = "benötigte Zeit")' >> $divplotdatei
    echo 'dev.off()' >> $divplotdatei
done
R --no-save < messwerte/multiplesuchbegriffe/mittelwert/divplot.r




# DIVIDIERE ALGORITHEMENLAUFZEITEN
if (( $INFO > 0 )); then echo "Algorithmenlaufzeiten werden dividiert"; fi

rkliste=()
wmliste=()
primitivliste=()

cd "$zielort"

# mittelwertdateien finden
for file in rkm*.$suchdaten
do
    rkliste+=("$file")
done
for file in wmm*.$suchdaten
do
    wmliste+=("$file")
done
for file in pm*.$suchdaten
do
    primitivliste+=("$file")
done


# sortiere die Dateien
IFS=$'\n'
GLOBIGNORE='*'
rkl=($(printf '%s\n' ${rkliste[@]} | awk '{ print length($0) " " $0; }' | sort -n | cut -d ' ' -f 2-))
wml=($(printf '%s\n' ${wmliste[@]} | awk '{ print length($0) " " $0; }' | sort -n | cut -d ' ' -f 2-))
primitivl=($(printf '%s\n' ${primitivliste[@]} | awk '{ print length($0) " " $0; }' | sort -n | cut -d ' ' -f 2-))

if (( ${#rkl[@]} != ${#primitivl[@]} || ${#rkl[@]} != ${#wml[@]} ))
then
	# es ging was bei der Erzeugnung der Messwerte schief - SegFault z.B.
    echo "suchbegriffe für die mittelwerte sind nicht in gleicher anzahl vorhanden"
    exit 1
fi

cd ../../..

# gehe durch alle Mittelwertdateien und dividiere rk / primitiv
prog=rk/primitiv
echo "" > $divplotdatei
#echo "" > "$zielort"rk-primitiv.$suchdaten.div
for i in $(seq 1 ${#rkl[@]} )
do
    if (( $DEBUG > 2 )); then echo ./div "$zielort""${rkl[(($i-1))]}" "$zielort""${primitivl[(($i-1))]}"" > ""$zielort"rk-primitiv.$i.$suchdaten.div; fi
    ./div "$zielort""${rkl[(($i-1))]}" "$zielort""${primitivl[(($i-1))]}" > "$zielort"rk-primitiv.$i.$suchdaten.div

    # r skript vorbereiten
    echo "rkprimitiv = read.table"'("/home/jh/git/htwk/AlgorithmEngineering/Projekt/'"$zielort"rk-primitiv.$i.$suchdaten.div'"'', header = TRUE)' >> $divplotdatei
    echo 'pdf("/home/jh/git/htwk/AlgorithmEngineering/Projekt/graphen/div.rk-primitiv.'"$i.$suchdaten"'.pdf")' >> $divplotdatei
    echo 'plot(rkprimitiv$Time ~ rkprimitiv$Targetsize, rkprimitiv, main = "Methode: '"$prog"'", sub="Suchbegriffanzahl '"$i"'", xlab = "Zielgröße", ylab = "benötigte Zeit")' >> $divplotdatei
    echo 'dev.off()' >> $divplotdatei
done

# in pdf plotten
R --no-save < messwerte/multiplesuchbegriffe/mittelwert/divplot.r



# gehe durch alle Mittelwertdateien und dividiere rk / wm
prog=rk/wm
echo "" > $divplotdatei
for i in $(seq 1 ${#wml[@]} )
do
    if (( $DEBUG > 2 )); then echo ./div "$zielort""${rkl[(($i-1))]}" "$zielort""${wml[(($i-1))]}"" > ""$zielort"rk-wm.$i.$suchdaten.div; fi
    ./div "$zielort""${rkl[(($i-1))]}" "$zielort""${wml[(($i-1))]}" > "$zielort"rk-wm.$i.$suchdaten.div

    # r skript vorbereiten
    echo "rkwm = read.table"'("/home/jh/git/htwk/AlgorithmEngineering/Projekt/'"$zielort"rk-wm.$i.$suchdaten.div'"'', header = TRUE)' >> $divplotdatei
    echo 'pdf("/home/jh/git/htwk/AlgorithmEngineering/Projekt/graphen/div.rk-wm.'"$i.$suchdaten"'.pdf")' >> $divplotdatei
    echo 'plot(rkwm$Time ~ rkwm$Targetsize, rkwm, main = "Methode: '"$prog"'", sub="Suchbegriffanzahl '"$i"'", xlab = "Zielgröße", ylab = "benötigte Zeit")' >> $divplotdatei
    echo 'dev.off()' >> $divplotdatei
done

# in pdf plotten
R --no-save < messwerte/multiplesuchbegriffe/mittelwert/divplot.r
