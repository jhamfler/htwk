#define DEBUG 0
#include <stdio.h>
#include <string.h> // strlen
#include "../hilfswerkzeuge/zeitmessungm.c" // Zeit messen
#include "mwm.c"

void search(char *pat[], char txt[], int anzahlsuchbegriffe);

int main(int argc, char * argv[])
{
	if(argc < 3) {
		//					1	 2			3
		printf ("Benutzung: ./rk Quelldatei Suchstring1 Suchstring2 ...");
		return EXIT_FAILURE;
	}

	char *suchstrings[argc-2];
	for (int i=0; i<argc-2; i++) {
			suchstrings[i] = argv[i+2];
	}

	long long t=0;
	t=zeitmessung(&search, argv[1], suchstrings, argc-2);
	printf("%lld\n",t);

	return EXIT_SUCCESS;
}

void search(char ** pat, char txt[], int anzahlsuchbegriffe) {
   unsigned char *T, *text;
   unsigned long long textlen;
   int            nmatches, i, bm = 0;
   MWM_STRUCT    *ps;
   int            npats=0, len, stat, nocase=0;
   BINARY        *p;
   int            irep=0,irand=0,isyn=0,nrep=1024,repchar=0;

   /* -- Allocate a Pattern Matching Structure - and Init it. */
   ps = mwmNew();

	text = txt;
	textlen=strlen(txt);
//	if (DEBUG) printf("text=%d\ttextlen=%lld",text,textlen);
	npats=anzahlsuchbegriffe;

	for (int i=0; i<npats; i++) {
		len=strlen(pat[i]);
		mwmAddPatternEx(ps, (unsigned char*)pat[i], len, nocase, 0, 0, (void*)i /* ID */, 3000 /* IID -internal id*/ );
	}

	mwmPrepPatterns( ps );
	stat = mwmSearch( (void*)ps, (unsigned char*) txt, textlen, match, 0 );

	if( stat == 0 ) {
		if(DEBUG) printf("no pattern matches\n");
	} else {
		if(DEBUG) printf("%d pattern matches in list\n",stat);
	}
}
