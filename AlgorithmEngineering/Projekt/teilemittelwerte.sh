#!/bin/bash

declare -a algorithmen=("rk" "primitiv")
#suchdaten=gen # a-z 20-240 mb
#suchdaten=gen2 # a-b 12-290 mb
#suchdaten=gen3 # a-z 100-2500 kb
suchdaten=science
#suchdaten=txt

#suchbegriffe=("a" "to" "the" "from" "would" "people" "because" "although" "important" "abnormally")
suchbegriffe=("u" "bc" "cci" "loux" "yxsua" "ibxdoy" "khvsosz" "xpzmhhab" "lkzxpyqwv" "yncqbjppiy" "zmavvqvwbis" "eaqastuvltot" "gqodykabpqkyo" "zrvffhkziyrmfj" "kvjkysupofohkpi" "lcqbohvnaalhyyko" "uhlppqbwqtbppezxr" "ewqnawdokgbfdnjuvl" "gdaegemnyhqqhktulew" "nhgxseqtwnwwrzboxdpy")


rkliste=()
primitivliste=()
zielort=messwerte/mittelwerte/
divplotdatei="$zielort""divplot.r"
prog=rk/primitiv

cd "$zielort"

# mittelwertdateien finden
for file in rk.*.$suchdaten
do
	rkliste+=("$file")
done

for file in primitiv.*.$suchdaten
do
	primitivliste+=("$file")
done


IFS=$'\n'
GLOBIGNORE='*'
rkl=($(printf '%s\n' ${rkliste[@]} | awk '{ print length($0) " " $0; }' | sort -n | cut -d ' ' -f 2-))
primitivl=($(printf '%s\n' ${primitivliste[@]} | awk '{ print length($0) " " $0; }' | sort -n | cut -d ' ' -f 2-))


if (( ${#rkl[@]} != ${#primitivl[@]} ))
then
	echo "suchbegriffe für die mittelwerte sind nicht in gleicher anzahl vorhanden"
	exit 1
fi

cd ../..

# gehe über alle mittelwertdateien und dividiere primitiv durch rk
echo "" > $divplotdatei
#echo "" > "$zielort"rk-primitiv.$suchdaten.div
for i in $(seq 1 ${#rkl[@]})
do
	echo ./div "$zielort""${rkl[$i]}" "$zielort""${primitivl[$i]}"
	./div "$zielort""${rkl[$i]}" "$zielort""${primitivl[$i]}" > "$zielort"rk-primitiv.$i.$suchdaten.div

	# r skript vorbereiten
	suchbegriff="Länge = $i"
	echo "rkprimitiv = read.table"'("/home/jh/git/htwk/AlgorithmEngineering/Projekt/'"$zielort"rk-primitiv.$i.$suchdaten.div'"'', header = TRUE)' >> $divplotdatei
	echo 'pdf("/home/jh/git/htwk/AlgorithmEngineering/Projekt/graphen/div.rk-primitiv.'"$suchdaten"'.pdf")' >> $divplotdatei
	echo 'plot(rkprimitiv$Time ~ rkprimitiv$Targetsize, rkprimitiv, main = "Methode: '"$prog"'", sub="Suchbegrifflänge 1-20''", xlab = "Zielgröße", ylab = "benötigte Zeit")' >> $divplotdatei
	echo 'dev.off()' >> $divplotdatei

done


#for e in "${rkl[@]}"
#do
#	echo $e
#done
#for e in "${primitivl[@]}"
#do
#	echo $e
#done
