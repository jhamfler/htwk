#!/bin/bash
anzahlmessreihen=25
mkdir -p txt/gen
byte=1000
kilo=1000
mega=10
for i in $(seq 1 $anzahlmessreihen)
do
	base64 /dev/urandom | grep '[[:alpha:]]*' -o | grep '[[:lower:]]' -o | tr -d '\n' | head -c $((i*$mega*$kilo*$byte)) > "txt/gen/$i.txt"
done
