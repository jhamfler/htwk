#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>  // O_RDONLY
#include <sys/stat.h> // dateigröße stat
#include <stdlib.h> // malloc, EXIT_SUCCESS
#include <math.h>
#include "readfile.c"
#include "countlines.c"

#define DEBUG 0

int countlines(char *filename);
int vergleiche(const void* a, const void* b);

// benutzung: ./mw messwerte/messwertdatei > messwerte/mittelwerte
int main(int argc , char ** argv) {
	if (argc < 1) {
		perror("Zu wenig Argunente: mw eingabedatei");
		return EXIT_FAILURE;
	}

	char * filename = argv[1];
	int lines = countlines(filename)-1, i=0, j=0;
	if (DEBUG) printf("%d\n", lines);
	if (lines==0) {
		perror("Die Anzahl der Zeilen konnte nicht ermittelt werden.");
		return EXIT_FAILURE;
	}

	FILE *inf = fopen(filename, "r");
	char buffer[1000];
	char x[sizeof buffer];
	char y[sizeof buffer];

	long long messwerte [lines][2];

	i=-1;
	while (1) {
		if (fgets(buffer, sizeof buffer, inf) == NULL) {
			if (DEBUG) perror("EOF or IO\n");
			break;
		}
		if (sscanf(buffer, "%s %s", x, y) != 2) {
			if (DEBUG) perror("Format\n");
			break;
		}
		else {
			if (i==-1) printf("Targetsize\tTime\n"); // gebe Header aus
			else {
				if (DEBUG) printf("%lld, %lld\n", atoll(x) , atoll(y));
				messwerte[i][0] = atoll(x);
				messwerte[i][1] = atoll(y);
			}
		}
		i++;
	}
	fclose(inf);

	// verschiedene Werte identifizieren
	long long erster=messwerte[0][0];
	long anzahlmesswerte=0;
	for (i=0; i<lines; i++) {
		i++;
		if (erster==messwerte[i][0]){
			anzahlmesswerte=i; // Anzahl durch Dateigrößen bestimmen
			break;
		}
	}
	if (anzahlmesswerte==0) { // zweites Vorkommen nicht gefunden -> nur eine Messreihe
		anzahlmesswerte=lines;
		if (DEBUG) printf("lines: %d\n",lines-2);
	}

	// init Mittelwerte
	long long mittelwerte[anzahlmesswerte][2];
	for (i=0; i<anzahlmesswerte; i++) {
		mittelwerte[i][0]=0;
		mittelwerte[i][1]=0;
	}

	// Anzahl der Mesreihen bestimmen
	int messreihen=lines/anzahlmesswerte;
	for (i=0; i<anzahlmesswerte; i++) {
		mittelwerte[i][0] = messwerte[i][0]; // Dateigrößen
		for (j=0; j<messreihen; j++) {
			mittelwerte[i][1] = mittelwerte[i][1] + messwerte[i+j*anzahlmesswerte][1];
		}
		mittelwerte[i][1] = mittelwerte[i][1] / messreihen;
	}

	// Messreihen nach Größe sortieren
	qsort(mittelwerte, anzahlmesswerte, sizeof(mittelwerte[0]), vergleiche);

	long double laufzeit[anzahlmesswerte];
	for (i=0; i<anzahlmesswerte; i++) {
		laufzeit[i]=(long double)mittelwerte[i][1]/mittelwerte[i][0]; // lineare Laufzeit
	}



	// auf stdout ausgeben
	for (i=0; i<anzahlmesswerte; i++)
		printf("%lld\t%.15LF\n", mittelwerte[i][0], laufzeit[i]);

}

int vergleiche(const void* a, const void* b) {
     long long int_a = ( (long long**) a )[0];
     long long int_b = ( (long long**) b )[0];
     if ( int_a == int_b ) return 0;
     else if ( int_a < int_b ) return -1;
     else return 1;
}
