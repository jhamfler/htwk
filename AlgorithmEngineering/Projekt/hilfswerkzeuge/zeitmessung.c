#include "zeitmessung.h"
#include "readfile.c"

long long zeitmessung (void (*search)(char*,char*), char * pfad, char * muster){
    struct timespec startt, start2t, stopt;
    clockid_t clk_id;
//    clk_id = CLOCK_MONOTONIC;
    clk_id = CLOCK_MONOTONIC_RAW;
//    clk_id = CLOCK_PROCESS_CPUTIME_ID;
//    clk_id = CLOCK_THREAD_CPUTIME_ID;
//    clk_id = CLOCK_REALTIME;
//    clk_id = _POSIX_MONOTONIC_CLOCK;
//    clk_id = _POSIX_CPUTIME;
//    clk_id = _POSIX_THREAD_CPUTIME;

	long long t=0;

	char * pattern = muster;
	char * path = pfad;
	char * txt = ReadFile(path);

	// Startzeitpunkte
	clock_gettime(clk_id, &startt);
	clock_gettime(clk_id, &start2t);

	// Suche
	(*search)(pattern, txt);

	// Stopzeitpunkt
	clock_gettime(clk_id, &stopt);

	// Zeitunterschied berechnen
	t=((long long)start2t.tv_sec*1000000000+start2t.tv_nsec) - ((long long)startt.tv_sec*1000000000+startt.tv_nsec);
//        printf("\n start1: %lld\n",(long long)startt.tv_sec*1000000000+(long long)startt.tv_nsec);
//        printf(" start2: %lld\n",(long long)startt.tv_sec*1000000000+(long long)start2t.tv_nsec);
	t=((long long)stopt.tv_sec*1000000000+(long long)stopt.tv_nsec) - ((long long)startt.tv_sec*1000000000+(long long)startt.tv_nsec) - 2*t;
//        printf(" stop  : %lld\n",(long long)startt.tv_sec*1000000000+start2t.tv_nsec);
//        printf(" zeit   : %lld\n",t);
        //printf("%lld\n",t);
	return t;
}
