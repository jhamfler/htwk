#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>  // O_RDONLY
#include <sys/stat.h> // dateigröße stat
#include <stdlib.h> // malloc, EXIT_SUCCESS
#include "readfile.c"
#include "countlines.c"

#define DEBUG 0

int countlines(char *filename);
int vergleiche(const void* a, const void* b);

// benutzung: ./mw messwerte/messwertdatei > messwerte/mittelwerte
int main(int argc , char ** argv) {
	if (argc < 2) {
		perror("Zu wenig Argunente: div dividenddatei divisordatei");
		return EXIT_FAILURE;
	}

	char * filename1 = argv[1];
	char * filename2 = argv[2];
	int i=0;
	int lines1 = countlines(filename1) -1; // header nicht berücksichtigen
	int lines2 = countlines(filename2) -1;

	if (DEBUG) printf("l1: %d\tl2: %d", lines1, lines2);

	FILE *inf1 = fopen(filename1, "r");
	FILE *inf2 = fopen(filename2, "r");

	char buffer[1000];
	char x[sizeof buffer];
	char y[sizeof buffer];

	long long messwerte1 [lines1][2];
	long long messwerte2 [lines2][2];

	// 1. datei
	i=-1;
	while (1) {
		if (fgets(buffer, sizeof buffer, inf1) == NULL) {
			break;
		}
		if (sscanf(buffer, "%s %s", x, y) != 2) {
			break;
		}
		else {
			if (i==-1) printf("%s\t%s\n", x , y); // gebe header aus
			else {
				//printf("%lld, %lld\n", atoll(x) , atoll(y));
				messwerte1[i][0] = atoll(x);
				messwerte1[i][1] = atoll(y);
			}
		}
		i++;
	}
	fclose(inf1);

	// 2. datei
	i=-1;
	while (1) {
		if (fgets(buffer, sizeof buffer, inf2) == NULL) {
			break;
		}
		if (sscanf(buffer, "%s %s", x, y) != 2) {
			break;
		}
		else {
			if (i==-1) {} // 2. header wird ! benötigt
			else {
				//printf("%lld, %lld\n", atoll(x) , atoll(y));
				messwerte2[i][0] = atoll(x);
				messwerte2[i][1] = atoll(y);
			}
		}
		i++;
	}
	fclose(inf2);

	// prüfen ob reihenfolge der dateigrößen in den messwerten stimmt
	if (lines1 != lines2) {
		perror("Zeilenanzahl stimmt nicht überein");
		return EXIT_FAILURE;
	}
	for (i=0; i<lines1; i++) {
		if (DEBUG) printf("messwert1: %lld\tmesswert2: %lld\n", messwerte1[i][0], messwerte2[i][0]);
		if (messwerte1[i][0] != messwerte2[i][0]) {
			perror("Messwerte sind nicht in richtiger Reihenfolge. Mittelwerte der Messwerte müssen in richtiger Riehenfolge vorliegen / gleichartig sortiert sein.");
			return EXIT_FAILURE;
		}
	}

	// auf stdout ausgeben
	for (i=0; i<lines1; i++) {
		printf("%lld\t%.15LF\n", messwerte1[i][0], (long double)messwerte1[i][1]/(long double)messwerte2[i][1]);
	}
}
