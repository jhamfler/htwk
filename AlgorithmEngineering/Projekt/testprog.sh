#!/bin/bash
# testprog.sh progname suchstring
# ./testprog prog suchstring suchdaten zieldatei
# ./testprog.sh ./rk einlangersuchbegriff science messwerte/rk.einlangersuchbegriff.science

for file in txt/$3/*
do
	echo "$1" $file "$2" "$4"
	"$1" $file "$2" >> "$4"
done
