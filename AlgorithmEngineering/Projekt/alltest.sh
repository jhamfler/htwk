#!/bin/bash
# ./testtest.sh
# ./testprog prog suchstring suchdaten zieldatei
declare -a algorithmen=("rk" "primitiv" "p2")
#declare -a algorithmen=("p2")
anzahlsuchbegriffe=20 #10
anzahlmessreihen=25 #25
#suchdaten=gen # a-z 20-240 mb
#suchdaten=gen2 # a-b 12-290 mb
#suchdaten=gen3 # a-z 100-2500 kb
suchdaten=science
#suchdaten=txt

boxplotdatei="messwerte/boxplot.r"
mittelwertdatei="messwerte/mittelwert.r"
#declare -a suchbegriffe=("einlangersuchbegriff" "word")
#declare -a suchbegriffe=("$(base64 /dev/urandom | sed 's/[^a-z]*//g' | head -c $((10)) | tr '[:upper:]' '[:lower:]')" "$(base64 /dev/urandom | head -c $((2)) | tr '[:upper:]' '[:lower:]')")

# suchbegriffe generieren/bestimmen
#suchbegriffe=("a" "to" "the" "from" "would" "people" "because" "although" "important" "abnormally")
suchbegriffe=()
for i in $(seq 1 $anzahlsuchbegriffe)
do
        suchbegriffe+=("$(base64 /dev/urandom | grep '[[:alpha:]]*' -o | grep '[[:lower:]]' -o | tr -d '\n' | head -c $(($i)))")
done
echo ${suchbegriffe[@]}

mkdir -p messwerte/mittelwerte

# vergleichsgrapehen r skript vorbereiten
echo "" > $mittelwertdatei
suchbegrifflen=0
for suchbegriff in ${suchbegriffe[@]}
do
	suchbegrifflen=$(($suchbegrifflen + 1))

	echo 'pdf("/home/jh/git/htwk/AlgorithmEngineering/Projekt/graphen/vergleich.'"$suchbegrifflen"'.'"$suchdaten"'.pdf")' >> $mittelwertdatei

	# einlesen
	for prog in "${algorithmen[@]}"
	do
		echo "$prog = read.table(\"/home/jh/git/htwk/AlgorithmEngineering/Projekt/messwerte/mittelwerte/$prog.$suchbegriff.$suchdaten\", header = TRUE)" >> $mittelwertdatei
	done

	# punkte plotten
	i=1
	for prog in "${algorithmen[@]}"
	do
		if (( i == 1 )); then
			echo 'plot('"$prog"'$Time ~ '"$prog"'$Targetsize, rk, main = "rk, p1, p2", sub="Suchbegriff: '"$suchbegriff"'", xlab = "Zielgröße", ylab = "benötigte Zeit")' >> $mittelwertdatei
		else
			echo 'points('"$prog"'$Time ~ '"$prog"'$Targetsize)' >> $mittelwertdatei
		fi
		i=$(($i + 1))
	done

	# linien plotten
	echo 'lines(x=rk$Targetsize, y=rk$Time, lwd=1, lty="solid", col="green")' >> $mittelwertdatei
	echo 'lines(x=primitiv$Targetsize, y=primitiv$Time, lwd=1, lty="solid", col="blue")' >> $mittelwertdatei
	echo 'lines(x=p2$Targetsize, y=p2$Time, lwd=1, lty="solid", col="red")' >> $mittelwertdatei

	# speichern
	echo 'dev.off()' >> $mittelwertdatei
	echo "" >> $mittelwertdatei
done


echo "" > $boxplotdatei
for prog in "${algorithmen[@]}"
do
	suchbegrifflen=1
	for suchbegriff in ${suchbegriffe[@]}
	do
		# boxplots r skript vorbereiten
		echo "$prog = read.table"'("/home/jh/git/htwk/AlgorithmEngineering/Projekt/messwerte/'"$prog"'.'"$suchbegriff"'.'"$suchdaten"'", header = TRUE)' >> $boxplotdatei
		echo 'pdf("/home/jh/git/htwk/AlgorithmEngineering/Projekt/graphen/'"$prog"'.'"$suchbegrifflen"'.'"$suchdaten"'.pdf")' >> $boxplotdatei
		echo 'boxplot('"$prog"'$Time ~ '"$prog"'$Targetsize, '"$prog"', main = "Methode: '"$prog"'", sub="Suchbegriff: '"$suchbegriff"'", xlab = "Zielgröße", ylab = "benötigte Zeit")' >> $boxplotdatei
		echo 'dev.off()' >> $boxplotdatei
		echo "" >> $boxplotdatei

		# messwerte errechnen
		echo "Targetsize	Time" > messwerte/$prog.$suchbegriff.$suchdaten
		for i in $(seq 1 $anzahlmessreihen)
		do
			echo "./testprog.sh ./$prog $suchbegriff $suchdaten messwerte/$prog.$suchbegriff.$suchdaten"
			./testprog.sh ./$prog $suchbegriff $suchdaten messwerte/$prog.$suchbegriff.$suchdaten
		done

		# mittelwerte berechnen
		./mw "messwerte/$prog.$suchbegriff.$suchdaten" > "messwerte/mittelwerte/$prog.$suchbegriff.$suchdaten"

		suchbegrifflen=$(($suchbegrifflen + 1))
	done
done


echo ${suchbegriffe[@]}
