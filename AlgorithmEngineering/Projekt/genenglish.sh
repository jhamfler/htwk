#!/bin/bash

anzahlmessreihen=25
anzahlworte=1000 # pro messreihe
zielort=txt/geneng/

wortdatei=/usr/share/dict/american-english
anzahl=$(wc -l $wortdatei)


mkdir -p $zielort

for j in $(seq 1 $anzahlmessreihen)
do
	i=0
	echo "" > $zielort$j".txt"
	while (( $i < $anzahlworte * $j ))
	do
		nummer=$(od -N3 -An -i /dev/urandom | awk -v a="$anzahl" '{printf "%i\n", a * $1 / 16777216}')
		sed -n "$nummer""p" $wortdatei >> $zielort$j".txt"
		i=$(($i+1))
	done
done
