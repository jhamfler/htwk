#!/bin/bash
DEBUG=1
INFO=1
pinit=2
skip=0
wd="$(pwd)"

declare -a algorithmen=("rkm" "wmm" "pm")
#declare -a algorithmen=("rkm" "wmm")
#declare -a algorithmen=("p2")
suchdatenort=txt/
#anzahlsuchbegriffe=("10")
#anzahlsuchbegriffe=("6" "7" "8" "9")
anzahlsuchbegriffe=( "65" "70" "75" "80")
#anzahlsuchbegriffe=("100")
# latex erwähnt
anzahlmessreihen=30 #20
messzielpfad=messwerte/multiplesuchbegriffe/

# latex erwähnt
#suchdaten=gen # a-z 20-240 mb
#suchdaten=gen2 # a-b 12-290 mb
suchdaten=gen3 # a-z 100-2500 kb
#suchdaten=science
#suchdaten=alle # 2Gb Textdateien
#suchdaten=txt

suchbegrifflen=10
alphabet=
alphabet=gen
if [ -z $alphabet ]
then
	#alphabet=ngerman
	alphabet=american-english
	wortdatei=/usr/share/dict/$alphabet
	wortanzahl=$(cat $wortdatei | wc -l)
fi

if (( $skip < 1 ))
then
mkdir -p $messzielpfad"mittelwert/"
# messen
for anzahl in ${anzahlsuchbegriffe[@]}
do
	if (( $INFO > 0 )); then echo "Anzahl Suchbegriffe $anzahl"; fi
	# kopf für messwertdatei anlegen
	for algo in ${algorithmen[@]}
	do
		echo "Targetsize    Time" > $messzielpfad$algo.$anzahl.$suchdaten
	done

	# Messreihen durchgehen
    for i in $(seq 1 $anzahlmessreihen)
    do
		if (( $INFO > 0 )); then echo "Messreihe $i"; fi
		# generiere immer neue Suchdaten, damit die Suchwörter die Mittelwerte nicht beeinflussen
		suchbegriffe=()
		for i in $(seq 1 $anzahl)
		do
			# wähle korrektes Wörterbuch aus
			if [[ "$alphabet" == "gen" ]]
			then
				suchbegriffe+=("$(base64 /dev/urandom | grep '[[:alpha:]]*' -o | grep '[[:lower:]]' -o | tr -d '\n' | head -c $(($suchbegrifflen)))")
			else
				nummer=$(od -N3 -An -i /dev/urandom | awk -v a="$wortanzahl" '{printf "%i\n", a * $1 / 16777216}')
				suchbegriffe+=("$(sed -n ${nummer}p $wortdatei)")
			fi
		done
		if (( $INFO > 0 )); then echo "Suchbegriffe ${suchbegriffe[@]}"; fi

		# zu durchsuchenden Text durchgehen
		for file in $suchdatenort$suchdaten/*
		do
			# jeden Algorithmus auf jeden zu durchsuchden Text anwenden
			for algo in ${algorithmen[@]}
			do
	       	    if (( $DEBUG > 0 )); then echo "./$algo $file ${suchbegriffe[@]} >> $messzielpfad$algo.$anzahl.$suchdaten"; fi
				./$algo "$file" ${suchbegriffe[@]} >> $messzielpfad$algo.$anzahl.$suchdaten
			done
       	done
	done

    # mittelwerte berechnen
	if (( $INFO > 0 )); then echo "Mittelwerte werden berechnet"; fi
	for algo in ${algorithmen[@]}
	do
		if (( $DEBUG > 1 )); then echo "./mw $messzielpfad$algo.$anzahl.$suchdaten > $messzielpfad"mittelwert/"$algo.$anzahl.$suchdaten"; fi
		./mw $messzielpfad$algo.$anzahl.$suchdaten > $messzielpfad"mittelwert/"$algo.$anzahl.$suchdaten &
	done
	# auf alle berechnungen warten
	wait
done
fi



zielort=$messzielpfad"mittelwert/"
divplotdatei=$zielort"divplot.r"
algos=("rkm" "wmm") # rkm / wmm

if (( $skip < 2 ))
then
echo "" > $divplotdatei
for anzahl in ${anzahlsuchbegriffe[@]}
do
	./div $messzielpfad"mittelwert/"${algos[0]}.$anzahl.$suchdaten $messzielpfad"mittelwert/"${algos[1]}.$anzahl.$suchdaten > $messzielpfad"mittelwert/"${algos[0]}"-"${algos[1]}.$anzahl.$suchdaten.div

    # r skript vorbereiten
    echo "${algos[0]}${algos[1]} = read.table"'("/home/jh/git/htwk/AlgorithmEngineering/Projekt/'"$zielort"${algos[0]}-${algos[1]}.$anzahl.$suchdaten.div'"'', header = TRUE)' >> $divplotdatei
    echo 'pdf("/home/jh/git/htwk/AlgorithmEngineering/Projekt/graphen/div'.${algos[0]}-${algos[1]}.$anzahl."$suchdaten"'.pdf")' >> $divplotdatei
    echo "plot(${algos[0]}${algos[1]}"'$Time ~ '"${algos[0]}${algos[1]}"'$Targetsize, '"${algos[0]}${algos[1]}, main = "'"Methode: '${algos[0]}"/"${algos[1]}'", sub="Suchbegriffanzahl '"$anzahl"'", xlab = "Zielgröße [Byte]", ylab = "benötigte Zeit [ns]")' >> $divplotdatei
    echo 'dev.off()' >> $divplotdatei
done
R --no-save < messwerte/multiplesuchbegriffe/mittelwert/divplot.r
fi



boxplotdatei=$messzielpfad"boxplot.r"
suchlendatei=$messzielpfad"suchlen.r"

farben=("darkgreen" "blue" "red")
farbendeutsch=("grün" "blau" "rot")
lty=("1" "2" "3") # durchgängig, gestrichelt, gepunktet
ltyc=
legende=
ltydeutsch=("durchgängig" "gestrichelt" "gepunktet")
maintext=
untertitel=
ylim=
i=0

if (( $skip < 3 ))
then
for prog in "${algorithmen[@]}"
do
#	maintext="$maintext""$prog(${farbendeutsch[$i]} ${ltydeutsch[$i]}) "
#	maintext="$maintext""$prog "
#	untertitel="$untertitel""$prog(${ltydeutsch[$i]}) "
	[ ! -z "$legende" ] && legende="$legende"", "
	legende="$legende""\"$prog\""
	[ ! -z "$ltyc" ] && ltyc="$ltyc"", "
	ltyc="$ltyc""$(($i+1))"
	[ ! -z "$ylim" ] && ylim="$ylim"", "
	ylim="$ylim""$prog""[2]"
	i=$(($i + 1))
done
fi


if (( $skip < 4 ))
then
# vergleichsgrapehen r skript vorbereiten
echo "" > $suchlendatei
for anzahl in ${anzahlsuchbegriffe[@]}
do

	maintext="$anzahl Suchbegriff(e) in $suchdaten"
	echo 'pdf("'$wd'/graphen/vergleich.'"$anzahl"'.'"$suchdaten"'.pdf")' >> $suchlendatei

	# einlesen
	for prog in "${algorithmen[@]}"
	do
		echo "$prog = read.table(\"$wd/$zielort$prog.$anzahl.$suchdaten\", header = TRUE)" >> $suchlendatei
	done

	# punkte plotten
	i=1
	for prog in "${algorithmen[@]}"
	do
		if (( i == 1 )); then
			echo 'plot('"ylim=range($ylim), ""$prog"'$Time ~ '"$prog"'$Targetsize, '"$prog"', main = "'"$maintext"'", sub="'"${untertitel[@]}"'", xlab = "Zielgröße [Byte]", ylab = "benötigte Zeit [ns]")' >> $suchlendatei
		else
			echo 'points('"$prog"'$Time ~ '"$prog"'$Targetsize)' >> $suchlendatei
		fi
		i=2
	done

	# linien plotten
	i=0
	for prog in "${algorithmen[@]}"
	do
		echo 'lines(x='$prog'$Targetsize, y='$prog'$Time, lwd=1, '"lty=${lty[$i]}"')' >> $suchlendatei
		i=$(($i + 1))
	done
	#echo 'lines(x=p2$Targetsize, y=p2$Time, lwd=1, lty="solid", col="red")' >> $suchlendatei

	# legende hinzufügen
	echo 'legend(x="topleft", lty=c('"$ltyc"'), legend=c('"$legende"'))' >> $suchlendatei

	# speichern
	echo 'dev.off()' >> $suchlendatei
	echo "" >> $suchlendatei
done
R --no-save < $suchlendatei
fi
