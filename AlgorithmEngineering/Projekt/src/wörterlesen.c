#include <stdio.h>  // printf
#include <fcntl.h>  // O_RDONLY
#include <unistd.h> // Datei einlesen
#include <stdlib.h> // malloc, EXIT_SUCCESS
#include <ctype.h> // isalpha
//#include <sys/types.h> // isalpha
//#include <errno.h>


#define BUF_SIZE 8192


int readAllWords(char * path, char ** wordList);

int main (int argc, char *argv[]){
	char * wordlist;
	if(argc != 2) {
		printf ("Benutzung: ./tp Quelldatei");
		return 1;
	}

	if (readAllWords(argv[1], &wordlist) == 0) {

	}
	return (EXIT_SUCCESS);
}

int readAllWords(char * path, char ** wordList) {
	char buffer[BUF_SIZE];
	char * wort;
	int input_fd, output_fd, i=0, j=0, k, n;
	ssize_t bytes_in, bytes_out; // bytes gelesen, bytes geschrieben

	// öffne Quelldatei
	input_fd = open (path, O_RDONLY);
	if (input_fd == -1) {
		perror ("open");
		return 2;
	}

	while((bytes_in = read(input_fd, &buffer, BUF_SIZE)) > 0){
		printf("Puffergröße == %d, %i\n", (int) bytes_in, i);
		k=0; j=0; n=0; i=0;
		while(i < bytes_in) {
			if (isalpha(buffer[i])) {
				printf("%c", buffer[i]);
				j++;
			} else { // kein Wort
				if (j > 2) { // Wort gefunden
					printf(" %ld\n", sizeof(char)*j+1 );
					printf(" %c ", buffer[i]);
					// Wort vorhanden
					wort = (char *) malloc( (j+1) * sizeof(char) ); // Wortlänge + NULL
					printf("wort: %d", wort);
					if ( wort != NULL ) { // kopiere das Wort
							for (k=i-j, n=0; n<j; n++) {
								printf("i=%d, j=%d, k=%d, n=%d\n",i,j,k,n);
								wort[n] = buffer[k+n];
							}
							wort[1] = '\0'; // Ende des Worts
					} else {
						perror("malloc");
					}
				}
				j=0;
			}
			i++;
		}
		i=0;
	}
	return 0;
}
