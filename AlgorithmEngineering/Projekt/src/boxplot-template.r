primitiv = read.table("/home/jh/git/htwk/AlgorithmEngineering/Projekt/messwerte/primitiv.word.science", header = TRUE)
boxplot(primitiv$Time ~ primitiv$Targetsize, primitiv, main = "primitive Methode", xlab = "Zielgröße", ylab = "benötigte Zeit")

primitiv = read.table("/home/jh/git/htwk/AlgorithmEngineering/Projekt/messwerte/primitiv.einlangersuchbegriff.science", header = TRUE)
boxplot(primitiv$Time ~ primitiv$Targetsize, primitiv, main = "primitive Methode", xlab = "Zielgröße", ylab = "benötigte Zeit")

primitiv = read.table("/home/jh/git/htwk/AlgorithmEngineering/Projekt/messwerte/primitiv.word.gen", header = TRUE)
boxplot(primitiv$Time ~ primitiv$Targetsize, primitiv, main = "primitive Methode", xlab = "Zielgröße", ylab = "benötigte Zeit")

primitiv = read.table("/home/jh/git/htwk/AlgorithmEngineering/Projekt/messwerte/primitiv.einlangersuchbegriff.gen", header = TRUE)
boxplot(primitiv$Time ~ primitiv$Targetsize, primitiv, main = "primitive Methode", xlab = "Zielgröße", ylab = "benötigte Zeit")

rk = read.table("/home/jh/git/htwk/AlgorithmEngineering/Projekt/messwerte/rk.word.science", header = TRUE)
boxplot(rk$Time ~ rk$Targetsize, rk, main = "Rabin-Karp", xlab = "Zielgröße", ylab = "benötigte Zeit")

rk = read.table("/home/jh/git/htwk/AlgorithmEngineering/Projekt/messwerte/rk.einlangersuchbegriff.science", header = TRUE)
boxplot(rk$Time ~ rk$Targetsize, rk, main = "Rabin-Karp", xlab = "Zielgröße", ylab = "benötigte Zeit")

rk = read.table("/home/jh/git/htwk/AlgorithmEngineering/Projekt/messwerte/rk.word.gen", header = TRUE)
boxplot(rk$Time ~ rk$Targetsize, rk, main = "Rabin-Karp", xlab = "Zielgröße", ylab = "benötigte Zeit",varwidth=TRUE)

rk = read.table("/home/jh/git/htwk/AlgorithmEngineering/Projekt/messwerte/rk.einlangersuchbegriff.gen", header = TRUE)
boxplot(rk$Time ~ rk$Targetsize, rk, main = "Rabin-Karp", xlab = "Zielgröße", ylab = "benötigte Zeit",varwidth=TRUE)
