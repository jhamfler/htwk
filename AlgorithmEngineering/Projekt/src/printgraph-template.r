rk = read.table("/home/jh/git/htwk/AlgorithmEngineering/Projekt/messwerte/rk.c.gen", header = TRUE)
pdf("/home/jh/git/htwk/AlgorithmEngineering/Projekt/graphen/rk.c.gen.pdf")
boxplot(rk$Time ~ rk$Targetsize, rk, main = "Methode: rk", sub="Suchbegriff: c", xlab = "Zielgröße", ylab = "benötigte Zeit")
dev.off()
