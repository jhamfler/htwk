#include <stdio.h>  // printf
#include <fcntl.h>  // O_RDONLY
#include <unistd.h> // Datei einlesen
#include <stdlib.h> // malloc, EXIT_SUCCESS
#include <ctype.h> // isalpha
#include <math.h> // pow, log
#include <sys/stat.h> // dateigröße stat
#include <string.h> // strlen
//#include <sys/types.h> // isalpha
//#include <errno.h>

#define Q 7

int hash(char * string, int length);

int getPrim(int binprimlen, int maxlen);

int main (int argc, char *argv[]) {
	char * wort;
	char * pattern = argv[2];
	char nextbyte=0, c;
	int input_fd, patternhash, shash;
	unsigned int i=0, j=0, k, n;
	ssize_t bytes_in; // bytes gelesen, bytes geschrieben
	size_t patternlength=0;
	
	if(argc != 3) {
		printf ("Benutzung: ./rk Quelldatei Suchstring");
		return 1;
	}
	
  // öffne Quelldatei
  char * path = argv[1];
	input_fd = open(path, O_RDONLY);
	if (input_fd == -1) {
		perror ("open");
		return 2;
	}
  
  patternlength = strlen(pattern); // ganzer string ohne \0
  char buffer[patternlength]; // zum vergleich mit dem zu durchsuchenden Text
  printf("pat len %ld\n", patternlength);
  
  // lese größe der datei
	struct stat attribut;
	if (stat(*argv, &attribut) == -1) {
     fprintf(stderr,"Fehler bei stat....\n");
     return EXIT_FAILURE;
  } else {
     printf("Größe = %ld Bytes\n", attribut.st_size);
  }
  
  // bestimme primzahlbits
	int binprimlen = 2 * patternlength * attribut.st_size;
	for (i=0; i<sizeof(binprimlen); i++) {
	  if (binprimlen >> 1) {
	    binprimlen = sizeof(binprimlen-i);
	  }
	}
  binprimlen = (int)(log(binprimlen) / log(2))+1; // primzahlbitlänge
  
  
  int primzahl = getPrim(binprimlen, 2 * patternlength * attribut.st_size);
	
  
  
  
  // hash des suchstrings berechnen
  patternhash = hash(pattern, patternlength);
  
  // datei einlesen
  bytes_in = read(input_fd, &buffer, patternlength); // wegen nextbyte
  if (!(bytes_in > 0)) {
    return (EXIT_FAILURE);
  }
  /*
  // verschiebe alles 1 nach vorne
  for (i=patternlength; i>0; i--) {
    buffer[i]=buffer[i-1];
  }*/
  
  // rabin karp
  char e; // ersteres byte aus substring, h für "hallo"
  int phash; // letzter berechneter hash
  
  phash = hash(buffer, patternlength);
  
  while( (nextbyte = read(input_fd, &buffer, 1)) > 0) { // lese byteweise aus datei

    e = buffer[0]; // zu entfernendes Zeichen
	  
	  // füge neues byte dem substring hinzu
	  for (i=0; i<patternlength; i++)
	    buffer[i]=buffer[i+1];
	  buffer[patternlength-1] = nextbyte;
	  
	  shash = phash - ( ((int)e) * pow(2, patternlength) )*2 + ( (int)buffer[patternlength-1] );
	  shash = shash % Q;
	  
	  
    phash = shash; // aktueller hash wird zu letztem berechneten hash	  
	}
	
	return (EXIT_SUCCESS);
}

int hash(char * pattern, int l) { // l=5 wenn "hallo"
  int r=0, i;
  for (i=0; i<l; i++) {
    r += ((int)pattern[i]) * pow(2, l-i);
  }
  r=r%Q;
  return r;
}

int getPrim(int binprimlen, int maxlen) {
  // lese zufallszahlen ein
	// http://stackoverflow.com/questions/2572366/how-to-use-dev-random-or-urandom-in-c
	int randomData = open("/dev/random", O_RDONLY), i, prim;
  char myRandomData[binprimlen/sizeof(prim)+1];
  do {
    size_t randomDataLen = 0;
    while (randomDataLen < sizeof myRandomData) {
        ssize_t result = read(randomData, myRandomData + randomDataLen, (sizeof myRandomData) - randomDataLen);
        if (result < 0) {
            printf("error, unable to read /dev/random\n");
        }
        randomDataLen += result;
    }
    for (i=0; i<binprimlen; i++) {
      
    }
  } while (prim > maxlen);
  close(randomData);
  return prim;
}

