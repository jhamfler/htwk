#include<stdio.h>
#include<stdlib.h>

int main() {
	int size=0;
	char *array;

	printf("Array-Groesse eingeben: ");
	scanf("%d", &size);

	// Speicher reservieren
	array = (char *) malloc(size * sizeof(char));

	if(array != NULL) {
		printf("\nSpeicher ist reserviert\n");
	}else {
		printf("\nKein freier Speicher vorhanden.\n");
	}
	printf("%d",array);
	return 0;
}
